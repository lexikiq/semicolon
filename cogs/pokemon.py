import asyncio
import datetime
import os
import random
import re
import typing
from io import BytesIO

import discord
from PIL import Image
from aiohttp import ClientSession
from discord.ext import commands, tasks

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.errors import WebException
from cogs.utils.utils import JsonManager, embed_author_template


class PokemonData:
    __slots__ = {"name", "dex", "sprite", "error"}

    def __init__(self, name: str, dex: int, sprite: typing.Union[str, None], error: str = None):
        self.name = name
        self.dex = dex
        self.sprite = sprite
        self.error = error


missingno = PokemonData("MissingNo.", 0, "https://archives.bulbagarden.net/media/upload/9/98/Missingno_RB.png")


class PokemonManager(JsonManager):
    __slots__ = {"session", "i18n", "pool"}

    def __init__(self, session: ClientSession, localizer):
        super().__init__('pokemon.json')
        self.session = session
        self.i18n = localizer
        self.pool: dict[int, list[int]] = {}
        os.makedirs(CONSTANTS['pkmn_sprites_dir'], exist_ok=True)

    def init_data(self, key: str, init_with=None):
        if init_with is None:
            init_with = []
        super().init_data(key, init_with)

    async def pkmn(self, ctx: commands.Context, pokemon: typing.Union[str, int], endpoint: str = 'pokemon') -> PokemonData:
        pokemon = str(pokemon).lower()
        if pokemon in ['0', 'missingno', 'missingno.'] and endpoint == 'pokemon':
            return missingno
        async with self.session.get(f"https://pokeapi.co/api/v2/{endpoint}/{pokemon}") as r:
            if r.status == 200:
                data = await r.json()
            else:
                raise WebException(self.i18n, ctx, 'PokéAPI', r.status, await r.text())
        return PokemonData(data['name'].title(), data['id'], data['sprites']['front_default'])

    async def set_sprite(self, embed: discord.Embed, poke_data: PokemonData) -> tuple:
        output_name = 'poke{}.png'.format(poke_data.dex)
        savedir = CONSTANTS['pkmn_sprites_dir']
        output_path = os.path.join(savedir, output_name)
        file = None
        if poke_data.sprite:
            if output_name not in os.listdir(savedir):
                async with self.session.get(poke_data.sprite) as r:
                    if r.status != 200:
                        return embed, None
                    sprite_data = BytesIO(await r.read())
                sprite: Image.Image = Image.open(sprite_data)
                upscale = 3
                sprite = sprite.resize((sprite.width*upscale, sprite.height*upscale), Image.NEAREST)
                # P -> RGBA -> RGBa (fixes the issue of non-black transparent pixels not cropping) -> get mask of image
                bbox = sprite.convert(mode='RGBA').convert(mode='RGBa').getbbox()
                sprite.crop(bbox).save(output_path)
            file = discord.File(output_path, filename=output_name)
            embed.set_image(url="attachment://" + output_name)
        return embed, file

    async def get_info(self, ctx: commands.Context, pokemon: str):
        poke_data = await self.pkmn(ctx, pokemon)
        embed = embed_author_template(ctx, subtle_author=None)
        embed.title = self.i18n.localize('pokemon_name', ctx=ctx).format(poke_data)
        embed.colour = discord.Colour(0xF93D32)
        embed, file = await self.set_sprite(embed, poke_data)
        if file is None:
            embed.description = self.i18n.localize('pokemon_no_sprite', ctx=ctx)

        kwargs = {'embed': embed}
        if file is not None:
            kwargs['file'] = file

        await ctx.send(file=file, embed=embed)

    async def catch(self, ctx):
        user_key = str(ctx.author.id)
        self.init_data(user_key)

        pool_key = ctx.channel.id
        if pool_key not in self.pool:
            pool = set()
            while len(pool) < CONSTANTS['pokemon_pool']:
                pool.add(random.randint(0, CONSTANTS['max_pokemon']))
            self.pool[pool_key] = list(pool)
        pool = self.pool[pool_key]

        has_pkmn = True
        rerolls = 0
        poke_data: PokemonData = missingno
        while has_pkmn and rerolls < CONSTANTS['pokemon_rerolls']:
            rerolls += 1
            poke_data = await self.pkmn(ctx, random.choice(pool))
            has_pkmn = poke_data.dex in self.data[user_key]
        embed = embed_author_template(ctx)
        embed, file = await self.set_sprite(embed, poke_data)
        embed.title = self.i18n.localize('pokemon_catch', ctx=ctx).format(self.i18n.localize('pokemon_name', ctx=ctx)
                                                                          .format(poke_data))
        if not has_pkmn:
            self.data[user_key].append(poke_data.dex)
            self.save()
            desc = self.i18n.localize('pokemon_new', ctx=ctx)
            clr = discord.Colour.green()
        else:
            desc = self.i18n.localize('pokemon_old', ctx=ctx)
            clr = discord.Colour.dark_green()

        if all(poke in self.data[user_key] for poke in pool):
            desc += f"\n\n*{self.i18n.localize('pokemon_all_old', ctx=ctx)}*"

        embed.description = desc
        embed.colour = clr

        kwargs = {'embed': embed}
        if file is not None:
            kwargs['file'] = file

        await ctx.send(**kwargs)

    def import_data(self):
        old_data_path = '../pkmn'
        for filename in (f for f in os.listdir(old_data_path) if re.match(r"\d{17,}\.txt", f)):
            filename_key = filename.replace('.txt', '')
            self.init_data(filename_key)
            userlist = self.data[filename_key]
            with open(os.path.join(old_data_path, filename), 'r') as f:
                dex_ids = f.readlines()
                for dex_id in dex_ids:
                    dex_id = int(dex_id)
                    if dex_id not in userlist:
                        userlist.append(dex_id)
        self.save()


class PokemonCog(commands.Cog, name='Pokemon'):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.pokemon_manager = PokemonManager(bot.session, bot.i18n)
        self.clear_pools.start()

    @commands.group(invoke_without_command=True, aliases=['pkmn', 'pokémon', 'poke', 'poké'])
    @commands.cooldown(rate=1, per=3, type=commands.BucketType.channel)
    @commands.bot_has_permissions(embed_links=True)
    async def pokemon(self, ctx: commands.Context, pokemon: str):
        """Get the information of a specified Pokémon.
        Accepts IDs or names.
        Powered by pokeapi.co."""
        async with ctx.typing():
            await self.pokemon_manager.get_info(ctx, re.sub(r"[^\w\d]", '', pokemon.lower()))

    @pokemon.group(name='catch', invoke_without_command=True)
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    @commands.bot_has_permissions(embed_links=True)
    async def pokemon_catch(self, ctx: commands.Context):
        """Catch a Pokémon!"""
        async with ctx.typing():
            await self.pokemon_manager.catch(ctx)

    @pokemon_catch.command(name='top')
    async def pokemon_catch_top(self, ctx: commands.Context):
        """Display the top Pokémon trainers."""
        embed = embed_author_template(ctx, True)
        embed.title = self.bot.i18n.localize('pokemon_top_title', ctx=ctx)

        top = sorted(self.pokemon_manager.data.items(), key=lambda x: len(x[1]), reverse=True)

        rank = -1
        prev_score = -1
        lines = []
        has_user = False
        line = self.bot.i18n.localize('top_msg', ctx=ctx) + self.bot.i18n.localize('pokemon_top_suffix', ctx=ctx)

        for i, (user_id, pkmn_list) in enumerate(top):
            # update score
            score = len(pkmn_list)
            if score != prev_score:
                rank = i + 1
            prev_score = score
            # check if user
            is_user = user_id == str(ctx.author.id)
            if is_user and i > 10:
                lines.append("...")
            elif not is_user and i > 9:
                continue
            has_user = has_user or is_user
            # create line
            record = {
                'count': rank,
                'score': score,
                'member': self.bot.get_user(int(user_id)) or user_id,
                'percent': score/(CONSTANTS['max_pokemon']+1)  # +1 for MissingNo.
            }
            lines.append(line.format(record))
            # abort if has user
            if has_user and i >= 9:
                break

        embed.description = '\n'.join(lines)
        await ctx.send(embed=embed)

    @pokemon.command(name='import')
    @commands.is_owner()
    async def pokemon_import(self, ctx: commands.Context):
        self.pokemon_manager.import_data()
        await ctx.send(self.bot.i18n.localize('imported', ctx=ctx))

    @tasks.loop(minutes=60.0)
    async def clear_pools(self):
        self.pokemon_manager.pool.clear()

    @clear_pools.before_loop
    async def before_tasks(self):
        now = datetime.datetime.now()
        plus_hour = now + datetime.timedelta(hours=1)
        hour_start = plus_hour.replace(minute=0, second=0, microsecond=0)
        await asyncio.sleep((hour_start - now).seconds)


async def setup(bot):
    await bot.add_cog(PokemonCog(bot))
