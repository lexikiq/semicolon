import typing
import feedparser
from dataclasses import dataclass
from datetime import datetime, timezone
from time import mktime
from html.parser import HTMLParser
import re
import pickle
import asyncio

import discord
from discord.ext import commands, tasks
from discord.utils import escape_markdown

from cogs.utils.bot import Semicolon
from cogs.utils.converters import SnowflakeConvert, TEXT_CHANNEL, ROLE
from cogs.utils.errors import SQLError
from cogs.utils.sql import TableManager
from cogs.utils.utils import embed_author_template
from cogs.utils.interactions import prompt, MenuOptionArgs, ButtonOptionArgs

XKCD_RSS = 'https://xkcd.com/rss.xml'
XKCD_LINK_REGEX = re.compile(r'https?://xkcd\.com/(\d+)/?')
YT_LINK_REGEX = re.compile(r'https?://(?:www\.)?youtube\.com/.+')

# prevent malicious actors from subscribing to a bajillion youtube channels
GUILD_SUBSCRIPTION_LIMIT = 100


class Dated(typing.Protocol):
    date: datetime


@dataclass
class XKCDFeedEntry:
    title: str
    number: int
    date: datetime
    link: str
    image_url: str
    alt_text: typing.Optional[str]


@dataclass
class YouTubeFeedEntry:
    date: datetime
    link: str


class SubscriptionManager(TableManager):
    def __init__(self, sql):
        super().__init__(sql,
            'subscriptions',
            [
            # Guild that the subscription is in
            "guild integer NOT NULL",
            # Channel that the subscription is in
            "channel integer NOT NULL",
            # Type of subscription (xkcd, YouTube, etc)
            "subscription_type text NOT NULL",
            # Additional differentiator for subscriptions (e.g. YouTube channel ID)
            "subscription_key text",
            # When the last subscribed item was posted
            "last_item_date real NOT NULL",
            # Additional data that doesn't need to be filtered by
            "data blob NOT NULL"],
            indices=[('guild', 'channel'), ('subscription_type', 'subscription_key')])


@dataclass
class SubscriptionData:
    ping_role: typing.Optional[int]
    name: str
    # Needed for listing subscriptions in the remove-subscription menu
    # (channel mentions don't work in menus)
    cached_channel_name: typing.Optional[str]


class TitleTextParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.alt_text: typing.Optional[str] = None
        self.src_url: typing.Optional[str] = None

    def handle_starttag(self, tag, attrs):
        if tag == 'img':
            try:
                self.alt_text = dict(attrs)['title']
                self.src_url = dict(attrs)['src']
            except KeyError:
                pass


T = typing.TypeVar('T', bound=Dated)
Subscription = typing.Tuple[int, int, str, typing.Optional[str], int, bytes]


class Subscriptions(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.subscription_manager = SubscriptionManager(bot.sql)
        self.process_subscriptions.start()

    @staticmethod
    async def post_xkcd(channel: 'discord.abc.Messageable', xkcd: XKCDFeedEntry, data: SubscriptionData):
        embed = discord.Embed(title=f'xkcd: {escape_markdown(xkcd.title)}', url=xkcd.link, timestamp=xkcd.date)
        embed.set_image(url=xkcd.image_url)
        embed.set_footer(text=xkcd.alt_text)
        return await channel.send(
            content=f'<@&{data.ping_role}>' if data.ping_role else None,
            embed=embed,
            allowed_mentions=discord.AllowedMentions(roles=[discord.Object(data.ping_role)] if data.ping_role else False))

    @staticmethod
    async def post_youtube(channel: 'discord.abc.Messageable', video: YouTubeFeedEntry, data: SubscriptionData):
        return await channel.send(
            content=(f'<@&{data.ping_role}> ' if data.ping_role else '') + video.link,
            allowed_mentions=discord.AllowedMentions(roles=[discord.Object(data.ping_role)] if data.ping_role else False))

    async def send_msgs(
            self,
            subscription: Subscription,
            entries: typing.Union[typing.List[T], typing.Dict[str, typing.List[T]]],
            handler: typing.Callable[['discord.abc.Messageable', T, SubscriptionData], typing.Coroutine[typing.Any, typing.Any, discord.Message]]):

        guild_id, channel_id, subscription_type, subscription_key, last_item_date_timestamp, subscription_data_bytes = subscription
        subscription_data = pickle.loads(subscription_data_bytes)

        if isinstance(entries, dict):
            if subscription_key is None:
                raise ValueError('entries is a dict but subscription_key is missing')
            feed = entries[subscription_key]
        else:
            feed = entries

        if len(feed) == 0:
            return

        guild = self.bot.get_guild(guild_id)
        if guild is None:
            return
        channel = guild.get_channel(channel_id)
        if channel is None or not isinstance(channel, discord.abc.Messageable):
            return
        perms = channel.permissions_for(guild.me)
        # TODO: provide a helpful error somewhere if missing perms
        if not (perms.read_messages and perms.send_messages):
            return

        last_item_date = datetime.fromtimestamp(last_item_date_timestamp, timezone.utc)
        new_entries: typing.List[T] = []
        for entry in feed:
            if entry.date <= last_item_date:
                break
            new_entries.append(entry)
        if len(new_entries) == len(feed):
            await channel.send(self.bot.i18n.localize_p('subscription_too_many_items', len(feed), channel=channel, guild=guild).format(len(feed)))

        new_entries.reverse()
        for entry in new_entries:
            message = await handler(channel, entry, subscription_data)
            if channel.type == discord.ChannelType.news:
                await message.publish()
            row_match = {
                'guild': guild_id,
                'channel': channel_id,
                'subscription_type': subscription_type
            }
            if subscription_key is not None:
                row_match['subscription_key'] = subscription_key

            subscription_data.cached_channel_name = channel.name
            self.subscription_manager.edit_row_by(row_match, (entry.date.timestamp(), pickle.dumps(subscription_data)), ['last_item_date', 'data'])

    async def handle_subscriptions(
            self,
            subscriptions: typing.List[Subscription],
            entries: typing.Union[typing.List[T], typing.Dict[str, typing.List[T]]],
            handler: typing.Callable[['discord.abc.Messageable', T, SubscriptionData], typing.Coroutine[typing.Any, typing.Any, typing.Any]]):

        if isinstance(entries, dict):
            for entries_list in entries.values():
                entries_list.sort(key=lambda entry: entry.date, reverse=True)
        else:
            entries.sort(key=lambda entry: entry.date, reverse=True)

        update_tasks: typing.List[asyncio.Task] = []
        for subscription in subscriptions:
            update_tasks.append(self.bot.loop.create_task(self.send_msgs(subscription, entries, handler)))

        return update_tasks

    async def process_xkcd_subscriptions(self, xkcd_subscriptions: typing.List[Subscription]):
        if len(xkcd_subscriptions) == 0:
            return []
        async with self.bot.session.get(XKCD_RSS) as r:
            body = await r.text()

        parsed_feed = feedparser.parse(body)
        xkcd_entries: typing.List[XKCDFeedEntry] = []
        for entry in parsed_feed['entries']:
            parser = TitleTextParser()
            parser.feed(entry['summary'])
            if parser.src_url is None:
                raise ValueError(f'Could not parse xkcd image from {entry["summary"]}')
            link_match = XKCD_LINK_REGEX.search(entry['link'])
            if not link_match:
                raise ValueError(f'Could not parse xkcd comic number from {entry["link"]}')

            xkcd_entries.append(XKCDFeedEntry(
                title=entry['title'],
                number=int(link_match.group(1)),
                date=datetime.fromtimestamp(mktime(entry['published_parsed']), timezone.utc),
                link=entry['link'],
                image_url=parser.src_url,
                alt_text=parser.alt_text))

        return await self.handle_subscriptions(xkcd_subscriptions, xkcd_entries, self.post_xkcd)

    async def process_youtube_subscriptions(self, youtube_subscriptions: typing.List[Subscription]):
        # TODO: this should be a SELECT DISTINCT but I don't feel like shoehorning that into the SQL API
        channel_ids = set(subscription[3] for subscription in youtube_subscriptions)
        feeds: typing.Dict[str, typing.List[YouTubeFeedEntry]] = {}
        for channel_id in channel_ids:
            channel_entries: typing.List[YouTubeFeedEntry] = []
            async with self.bot.session.get(f'https://www.youtube.com/feeds/videos.xml?channel_id={channel_id}') as r:
                body = await r.text()
            parsed_feed = feedparser.parse(body)
            for entry in parsed_feed['entries']:
                channel_entries.append(YouTubeFeedEntry(
                    date=datetime.fromtimestamp(mktime(entry['published_parsed']), timezone.utc),
                    link=entry['links'][0]['href']))
            feeds[channel_id] = channel_entries

        return await self.handle_subscriptions(youtube_subscriptions, feeds, self.post_youtube)

    @tasks.loop(minutes=5.0)
    async def process_subscriptions(self):
        await self.bot.wait_until_ready()
        xkcd_subscriptions = self.subscription_manager.get_rows_by({'subscription_type': 'xkcd'})
        youtube_subscriptions = self.subscription_manager.get_rows_by({'subscription_type': 'youtube'})

        xkcd_tasks = []
        try:
            xkcd_tasks = await self.process_xkcd_subscriptions(xkcd_subscriptions)
        except Exception as e:
            self.bot.logger.warn('Failed to process xkcd subscriptions', exc_info=e)

        youtube_tasks = []
        try:
            youtube_tasks = await self.process_youtube_subscriptions(youtube_subscriptions)
        except Exception as e:
            self.bot.logger.warn('Failed to process youtube subscriptions', exc_info=e)

        # Send out new subscriptions in different channels in parallel, ignoring any errors that may arise in
        # individual channels
        await asyncio.gather(*(xkcd_tasks + youtube_tasks), return_exceptions=True)

    @commands.group()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def subscribe(self, ctx: commands.Context['Semicolon']):
        """Manage posting subscriptions to various Internet data sources"""
        # Do this check here instead of enabling invoke_without_subcommand so that we run the permissions and guild
        # checks on every subcommand automatically
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    async def create_subscription(self, ctx: commands.Context['Semicolon'], *,
        channel: 'discord.abc.GuildChannel',
        subscription_type: str,
        subscription_key: typing.Optional[str] = None,
        ping_role: typing.Optional[discord.Role] = None,
        display_name: str,
        success_msg: str):
        if ctx.guild is None:
            raise ValueError('create_subscription called in non-guild context')

        # TODO: change to an SQL COUNT once the API is better
        guild_subscriptions = self.subscription_manager.get_rows_by({'guild': ctx.guild.id})
        if len(guild_subscriptions) >= GUILD_SUBSCRIPTION_LIMIT:
            await ctx.send(ctx.bot.i18n.localize('subscription_limit_reached', ctx=ctx).format(GUILD_SUBSCRIPTION_LIMIT))
            return

        rows_match = {
            'guild': ctx.guild.id,
            'channel': channel.id,
            'subscription_type': subscription_type
        }
        if subscription_key is not None:
            rows_match['subscription_key'] = subscription_key
        existing_subscriptions = self.subscription_manager.get_rows_by(rows_match)

        if len(existing_subscriptions) > 0:
            await ctx.send(ctx.bot.i18n.localize('subscription_already_exists', ctx=ctx).format(channel.mention))
            return

        self.subscription_manager.create_row((
            ctx.guild.id,
            channel.id,
            subscription_type,
            subscription_key,
            ctx.message.created_at.timestamp(),
            pickle.dumps(SubscriptionData(ping_role.id if ping_role else None, display_name, channel.name))))

        await ctx.send(success_msg)

    @subscribe.command(name='xkcd')
    async def subscription_add_xkcd(
        self,
        ctx: commands.Context['Semicolon'],
        channel: SnowflakeConvert(TEXT_CHANNEL),  # type: ignore
        ping_role: SnowflakeConvert(ROLE) = None):  # type: ignore
        '''Subscribe to XKCD in a given Discord channel. Optionally, ping the given role when a new comic is posted.'''
        # Please the typechecker
        if not ctx.guild:
            return
        channel = typing.cast('discord.TextChannel', channel)

        await self.create_subscription(
            ctx,
            channel=channel,
            subscription_type='xkcd',
            ping_role=ping_role,
            display_name='xkcd',
            success_msg=ctx.bot.i18n.localize('subscription_success_xkcd', ctx=ctx).format(channel.mention))

    @subscribe.command(name='youtube')
    async def subscription_add_yt(
        self,
        ctx: commands.Context['Semicolon'],
        channel: SnowflakeConvert(TEXT_CHANNEL),  # type: ignore
        yt_channel_link: str,
        ping_role: SnowflakeConvert(ROLE) = None):  # type: ignore
        """
        Subscribe to a YouTube channel in a given Discord channel.
        Optionally, ping the given role when a new video is posted.
        """
        # Please the typechecker
        if not ctx.guild:
            return
        channel = typing.cast('discord.TextChannel', channel)

        if not YT_LINK_REGEX.match(yt_channel_link):
            await ctx.send(ctx.bot.i18n.localize('subscription_youtube_invalid_channel_link', ctx=ctx))
            return

        async with self.bot.session.get(yt_channel_link) as r:
            body = await r.text()

        channel_id = None
        channel_name = None
        class YouTubePageParser(HTMLParser):
            def handle_starttag(self, tag, attrs):
                nonlocal channel_id, channel_name
                if tag == 'meta':
                    attr_dict = dict(attrs)
                    if attr_dict.get('itemprop', None) == 'channelId':
                        channel_id = attr_dict.get('content', None)
                    if attr_dict.get('property', None) == 'og:title':
                        channel_name = attr_dict.get('content', None)

        YouTubePageParser().feed(body)
        if channel_id is None:
            await ctx.send(ctx.bot.i18n.localize('subscription_youtube_channel_id_not_found', ctx=ctx))
            return

        await self.create_subscription(
            ctx,
            channel=channel,
            subscription_type='youtube',
            subscription_key=channel_id,
            ping_role=ping_role,
            display_name=channel_name if channel_name else channel_id,
            success_msg=ctx.bot.i18n.localize('subscription_success_youtube', ctx=ctx).format(channel_name, channel.mention))

    @subscribe.command(name='list')
    async def subscription_list(self, ctx: commands.Context['Semicolon']):
        """List all subscriptions in the server."""
        # Please the typechecker
        if not ctx.guild:
            return

        guild_subscriptions = self.subscription_manager.get_rows_by({'guild': ctx.guild.id}, cols=['channel', 'data'])

        def format_subscription(channel_id, subscription):
            if subscription.ping_role is None:
                return ctx.bot.i18n.localize('subscription_list_item', ctx=ctx).format(subscription.name, f'<#{channel_id}>')
            return ctx.bot.i18n.localize('subscription_list_item_with_ping', ctx=ctx)\
                .format(subscription.name, f'<#{channel_id}>', f'<@&{subscription.ping_role}>')

        embed = embed_author_template(ctx)
        embed.title = ctx.bot.i18n.localize('subscription_list_title', ctx=ctx)
        embed.description = '\n'.join([
            format_subscription(channel_id, subscription) for channel_id, subscription in (
                (channel_id, pickle.loads(row)) for channel_id, row in guild_subscriptions)])

        await ctx.send(embed=embed)

    @subscribe.command(name='remove')
    async def subscription_remove(self, ctx: commands.Context['Semicolon']):
        """Remove a subscription from the server."""
        # Please the typechecker
        if not ctx.guild:
            return

        guild_subscriptions = self.subscription_manager.get_rows_by({'guild': ctx.guild.id})
        subscriptions_data = [pickle.loads(row) for _, _, _, _, _, row in guild_subscriptions]

        choice = await prompt(
            ctx,
            ctx.bot.i18n.localize('subscription_remove_desc', ctx=ctx),
            menu_options=[MenuOptionArgs(
                str(i),
                subscription.name,
                f'#{subscription.cached_channel_name}' if subscription.cached_channel_name else None
            ) for i, subscription in enumerate(subscriptions_data)],
            button_options=[ButtonOptionArgs('cancel', ctx.bot.i18n.localize('cancel_action', ctx=ctx))]
        )
        if choice is None or choice == 'cancel':
            return

        subscription = guild_subscriptions[int(choice)]
        try:
            guild_id, channel_id, subscription_type, subscription_key, _, _ = subscription
            rows_match = {
                'guild': guild_id,
                'channel': channel_id,
                'subscription_type': subscription_type
            }
            if subscription_key is not None:
                rows_match['subscription_key'] = subscription_key
            self.subscription_manager.delete_row_by(rows_match)
        except SQLError:
            # someone else deleted the subscription out from under us while waiting for the prompt
            pass


async def setup(bot):
    await bot.add_cog(Subscriptions(bot))
