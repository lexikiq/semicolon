import typing

import discord
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.converters import SnowflakeConvert, ROLE
from cogs.utils.utils import get_roles_by_prefix, is_stys, toggle_role
from cogs.utils.interactions import prompt, MenuOptionArgs
from cogs.utils.sql import GuildManager, SQLError
from sqlite3 import IntegrityError

class FlairManager(GuildManager):
    def __init__(self, sql, snowflake: int):
        super().__init__(sql,
                         'flairs',
                         ["flair_id integer PRIMARY KEY", "description text"],
                         snowflake)

class Roles(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot

    @commands.group(invoke_without_command=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.check(is_stys)
    async def stream(self, ctx):
        """
        Toggles on/off a stream notification role.
        """
        prefix = 'stream-'

        items: typing.List[discord.Role] = get_roles_by_prefix(prefix, ctx.guild)
        if not items:
            await ctx.send(self.bot.i18n.localize('no_stream', ctx=ctx).format(prefix))
            return
        items = list(sorted(items, key=lambda x: x.name.lower()))

        role_choice = await prompt(
            ctx,
            self.bot.i18n.localize('stys_stream_desc', ctx=ctx),
            menu_options=[MenuOptionArgs(str(role.id), role.name) for role in items]
        )
        if role_choice is None:
            return
        role = next(role for role in items if role.id == int(role_choice))
        await toggle_role(ctx.author, role)

    def get_flair_manager(self, guild):
        if isinstance(guild, discord.Guild):
            guild = guild.id
        return FlairManager(self.bot.sql, guild)

    @commands.group(invoke_without_command=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.guild_only()
    async def flair(self, ctx: commands.Context):
        """
        Toggle a "flair" role if supported by the server.
        """

        flairs = self.get_flair_manager(ctx.guild).get_rows()

        # Filter out any roles that have been deleted since being added as flairs
        flair_roles_unfiltered = ((discord.utils.get(ctx.guild.roles, id=flair[0]), flair[1]) for flair in flairs)
        flair_roles = [typing.cast(typing.Tuple[discord.Role, str], flair) for flair in flair_roles_unfiltered if flair[0] is not None]
        # Sort by role position
        flair_roles.sort(key=lambda flair: flair[0].position, reverse=True)

        if not flair_roles:
            await ctx.send(self.bot.i18n.localize('no_flair', ctx=ctx))
            return

        items: typing.List[discord.Role] = [flair[0] for flair in flair_roles]
        itemtext = [flair[0].mention + (" ({})".format(flair[1]) if flair[1] is not None else "") for flair in flair_roles]

        roles_on_author = set(role.id for role in ctx.author.roles)

        role_choice = await prompt(
            ctx,
            self.bot.i18n.localize('flair_select_desc', ctx=ctx),
            menu_options=[MenuOptionArgs(str(flair_role.id), flair_role.name, description) for flair_role, description in flair_roles])
        if role_choice is None:
            return
        role = next(flair_role for flair_role, _ in flair_roles if flair_role.id == int(role_choice))

        existing_role = discord.utils.get(ctx.author.roles, id=role.id)

        try:
            if existing_role is not None:
                    await ctx.author.remove_roles(existing_role)
            else:
                await ctx.author.add_roles(role)
        except discord.Forbidden:
            await ctx.send(self.bot.i18n.localize('flair_toggle_too_high', ctx=ctx))

    async def make_existing_role_flair(self, ctx: commands.Context, role: discord.Role, description):
        """Utility function to mark an existing role as a "flair" role."""
        try:
            role = discord.utils.get(ctx.guild.roles, id=role.id)
            if role is None:
                await ctx.send(self.bot.i18n.localize('flair_add_nonexists', ctx=ctx))
                return

            if role > ctx.guild.me.top_role:
                await ctx.send(self.bot.i18n.localize('flair_add_too_high', ctx=ctx))
                return

            self.get_flair_manager(ctx.guild).create_row((role.id, description))
            await ctx.send(self.bot.i18n.localize('flair_add_success', ctx=ctx).format(role.mention))
        except IntegrityError as e:
            await ctx.send(self.bot.i18n.localize('flair_add_exists', ctx=ctx).format(role.mention))
            return

    @flair.command(name='add')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def flair_add(self, ctx: commands.Context, flair: SnowflakeConvert(ROLE), *, description: str = None):
        """Add an existing role to the list of self-assignable "flair" roles."""

        await self.make_existing_role_flair(ctx, flair, description)

    @flair.command(name='create')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def flair_create(self, ctx: commands.Context, flair_name: str, *, description: str = None):
        """Create a new self-assignable "flair" role."""
        existing_role = discord.utils.get(ctx.guild.roles, name=flair_name)
        if existing_role:
            await self.make_existing_role_flair(ctx, existing_role, description)
            return

        role = await ctx.guild.create_role(name=flair_name,
                                    reason=self.bot.i18n.localize('reason', guild=ctx.guild).format(
                                        ctx.author))
        self.get_flair_manager(ctx.guild).create_row((role.id, description))
        await ctx.send(self.bot.i18n.localize('flair_create_success', ctx=ctx).format(role.mention))

    @flair.command(name='remove')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def flair_remove(self, ctx: commands.Context, flair: SnowflakeConvert(ROLE)):
        """Remove a role from the list of self-assignable "flair" roles."""
        try:
            self.get_flair_manager(ctx.guild).delete_row_by({'flair_id': flair.id})
        except SQLError as e:
            await ctx.send(self.bot.i18n.localize('flair_remove_nonexists', ctx=ctx).format(flair.mention))
            return

        await ctx.send(self.bot.i18n.localize('flair_remove_success', ctx=ctx).format(flair.mention))

    @flair.command(name='delete')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def flair_delete(self, ctx: commands.Context, flair: SnowflakeConvert(ROLE)):
        """Delete a self-assignable "flair" role entirely."""
        try:
            self.get_flair_manager(ctx.guild).delete_row_by({'flair_id': flair.id})

            role = discord.utils.get(ctx.guild.roles, id=flair.id)
            role_name = role.name
            await role.delete(reason=self.bot.i18n.localize('reason', guild=ctx.guild).format(ctx.author))

            await ctx.send(self.bot.i18n.localize('flair_delete_success', ctx=ctx).format(role_name))
        except SQLError as e:
            await ctx.send(self.bot.i18n.localize('flair_remove_nonexists', ctx=ctx).format(flair.mention))
            return

    @flair.command(name='description', aliases=['desc'])
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def flair_description(self, ctx: commands.Context, flair: SnowflakeConvert(ROLE), *, description: str = None):
        """Update the description of a "flair" role"""
        self.get_flair_manager(ctx.guild).edit_row_by({'flair_id': flair.id}, (flair.id, description))
        await ctx.send(self.bot.i18n.localize('flair_description_success', ctx=ctx))


    @commands.command(aliases=['colour'])
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.guild_only()
    async def color(self, ctx: commands.Context):
        """
        Selects a role color if supported by the server.
        Searches for roles prefixed with "color-"
        Optionally, for mods (defined as having "Manage Messages" permissions), searches for roles prefixed with "scolor-"
        """
        default_prefix = 'color-'
        staff_prefix = 's'+default_prefix

        au_perms = ctx.author.guild_permissions
        is_mod = au_perms.administrator or au_perms.manage_messages or ctx.author == ctx.guild.owner
        top_role = ctx.guild.me.top_role

        prefix = staff_prefix if is_mod else default_prefix

        items: typing.List[discord.Role] = get_roles_by_prefix(prefix, ctx.guild)
        if not items and prefix != default_prefix:
            prefix = default_prefix
            items = get_roles_by_prefix(prefix, ctx.guild)
        if not items:
            await ctx.send(self.bot.i18n.localize('no_color', ctx=ctx).format(default_prefix))
            return

        menu_options = [MenuOptionArgs(str(role.id), role.name, preview_text=role.mention) for role in items]

        existing_role = discord.utils.find(lambda x: x.name.startswith(prefix) and top_role > x, ctx.author.roles)
        if existing_role is not None:
            color_remove_text = self.bot.i18n.localize('color_remove', ctx=ctx)
            menu_options.insert(0, MenuOptionArgs('color_remove', color_remove_text, preview_text=color_remove_text))

        role_choice = await prompt(
            ctx,
            self.bot.i18n.localize('color_select_desc', ctx=ctx),
            menu_options=menu_options)
        if role_choice is None:
            return
        if role_choice == "color_remove":
            if existing_role is not None:
                await ctx.author.remove_roles(existing_role)
            return

        role = next((role for role in items if role.id == int(role_choice)), None)

        if existing_role == role:
            return
        if existing_role is not None:
            await ctx.author.remove_roles(existing_role)
        if role:
            await ctx.author.add_roles(role)


async def setup(bot):
    await bot.add_cog(Roles(bot))
