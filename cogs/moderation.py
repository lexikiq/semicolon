import asyncio
import datetime
import json
import os
import re
import typing
from dataclasses import dataclass
from enum import Enum

import discord
import humanize
from discord.ext import commands, tasks
from discord.utils import utcnow

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.converters import SnowflakeConvert, MEMBER, ROLE, EMOJI, BaseConverter, List, TimeConvert, ConvertedTime
from cogs.utils.errors import SQLError, VariableMissing, VariableParseError, VariableError, \
    VariableTypeError, ExpectedError, UserInputError, HierarchyError
from cogs.utils.sql import TableManager, GuildManager
from cogs.utils.time import unmute
from cogs.utils.utils import data_path, shorten, \
    add_attachment_info, is_stys, comma_separator, line_split
from cogs.utils.interactions import ButtonOptionArgs, MenuOptionArgs, prompt
from cogs.utils.variables import FakeContext, display_variable

H0NDE = re.compile(r"[^\w/.]", re.I)

class QuickAction(typing.NamedTuple):
    button_style: discord.ButtonStyle
    button_label: str

class QuickActions(Enum):
    value: QuickAction
    DELETE = QuickAction(button_style=discord.ButtonStyle.danger, button_label='report_modmail_quick_action_delete')
    KICK = QuickAction(button_style=discord.ButtonStyle.danger, button_label='report_modmail_quick_action_kick')
    BAN = QuickAction(button_style=discord.ButtonStyle.danger, button_label='report_modmail_quick_action_ban')
    DISMISS = QuickAction(button_style=discord.ButtonStyle.secondary, button_label='report_modmail_quick_action_dismiss')

async def clear_buttons(interaction: discord.Interaction, button_id: typing.Optional[str] = None):
    await interaction.response.defer()
    if interaction.message is None:
        return
    view = discord.ui.View.from_message(interaction.message)
    for item in view.children:
        if isinstance(item, discord.ui.Button) and (item.custom_id == button_id or button_id is None):
            view.remove_item(item)

    await interaction.message.edit(view=view)

class ScoreType(Enum):
    TOXICITY = (1, {"en", "es", "fr", "de", "pt", "it", "ru"})
    SEVERE_TOXICITY = (2, {"en", "fr", "es", "de", "it", "pt", "ru"})
    IDENTITY_ATTACK = (3, {"en", "de", "it", "pt", "ru"})
    INSULT = (4, {"en", "de", "it", "pt", "ru"})
    PROFANITY = (5, {"en", "de", "it", "pt", "ru"})
    THREAT = (6, {"en", "de", "it", "pt", "ru"})

    __slots__ = {"langs"}

    def __init__(self, index, langs):
        self.langs = langs


class AnalyzeResults:
    __slots__ = {"_result_map", "_i"}

    def __init__(self, result_map: typing.Dict[ScoreType, float]):
        for result, value in result_map.items():
            if not isinstance(result, ScoreType):
                raise Exception("AnalyzeResults dict values must be ScoreType")
            if not isinstance(value, float):
                raise Exception("AnalyzeResults dict values must be float")
        self._result_map = result_map

    def __getitem__(self, item: ScoreType) -> float:
        return self._result_map[item]

    def __iter__(self):
        self._i = 0
        return self

    def __next__(self):
        map_iter = list(self._result_map.items())
        if self._i < len(map_iter):
            return map_iter[self._i]
        else:
            raise StopIteration

    @classmethod
    def from_json(cls, json_data: typing.Dict[str, typing.Any]):
        scores = json_data['attributeScores']
        result_data = {}
        for type_obj, score_obj in scores.items():
            result_data[ScoreType[type_obj]] = score_obj['summaryScore']['value'] * 100
        return AnalyzeResults(result_data)


@dataclass
class CacheValue:
    created: datetime.datetime
    messages: typing.Dict[int, discord.Message]
    updated: datetime.datetime = None
    lock: asyncio.Lock = asyncio.Lock()

    def get_updated(self):
        return self.updated or self.created


@dataclass(frozen=True)
class CacheKey:
    author: int
    guild: int


@dataclass(frozen=True)
class SpamKey(CacheKey):
    message: str


class RuleManager(GuildManager):
    def __init__(self, sql, snowflake: int):
        super().__init__(sql,
                         'rules',
                         ["rule text NOT NULL", "ping_here integer NOT NULL"],
                         snowflake)


class Moderation(commands.Cog):
    report_match = re.compile(r"https://discord.com/channels/(\d{17,19})/(\d{17,19})/(\d{17,19})")
    id_match = re.compile(r"(\d{17,19})")

    mod_config_options = {
        "muted_role": SnowflakeConvert(ROLE, allow_partial=False),
        "join_role": SnowflakeConvert(ROLE, allow_partial=False),
        "report_emoji": SnowflakeConvert(EMOJI, allow_partial=False),
        "prefix": List(str),
        "ping_ban_threshold": int,
        "ping_ban_join_bypass": int,
        "ping_ban_creation_bypass": int,
        "misbehavior_threshold": int
    }

    enabled_attributes: typing.Set[ScoreType] = {
        ScoreType.TOXICITY,
        ScoreType.SEVERE_TOXICITY
    }

    spam_cache_expire = datetime.timedelta(seconds=20)
    persp_cache_expire = datetime.timedelta(seconds=40)

    def __init__(self, bot: Semicolon):
        self.bot = bot
        self._ = bot.i18n.localize
        self._p = bot.i18n.localize_p

        for k, v in self.mod_config_options.items():
            self.bot.config_options[k] = v

        self.bot.register_consumer("netban", self.consumer_handler)

        self.gtoken = None
        if gtoken := self.bot.bot_config['perspective']['key']:
            self.gtoken = gtoken

        self.perspective_langs: typing.Set[str] = None
        for score_type in self.enabled_attributes:
            if self.perspective_langs is None:
                self.perspective_langs = set(score_type.langs)
            else:
                self.perspective_langs = set(
                    x for x in self.perspective_langs if x in score_type.langs)

        self.perspective_cache: typing.Dict[CacheKey, CacheValue] = {}
        self.spam_cache: typing.Dict[SpamKey, CacheValue] = {}

        self.cache_cleaner.start()

    def cog_unload(self):
        self.cache_cleaner.cancel()

    async def analyze_comment(self, msg: str, langs: typing.Collection[str] = None, guild_id: int = None,
                              context: typing.List[discord.Message] = None) -> typing.Optional[AnalyzeResults]:
        if langs is None:
            langs = ["en"]
        else:
            langs = list(set(langs))
        attrs = {x.name: {} for x in self.enabled_attributes}
        json_obj = {"comment": {"text": msg, "type": "PLAIN_TEXT"}, "languages": langs,
                    "requestedAttributes": attrs, "doNotStore": True}
        if guild_id is not None:
            json_obj['communityId'] = str(guild_id)
        if context is not None:
            ctx = []
            for msg in context:
                ctx.append({"text": msg.system_content, "type": "PLAIN_TEXT"})
            json_obj['context'] = {"entries": ctx}
        request = json.dumps(json_obj, separators=(',', ':'))
        async with self.bot.session.post(
                "https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze",
                headers={"User-Agent": "semicolon-bot:v1.2", "Content-Type": "application/json"},
                params={"key": self.gtoken}, data=request) as r:
            rjson = await r.json()
            if 'attributeScores' not in rjson or r.status != 200:
                if 'error' in rjson:
                    err = rjson['error']
                    self.bot.logger.warn(f"Perspective API: {err['code']} {err['status']} -- {err['message']}")
                else:
                    self.bot.logger.error(f"Perspective API: Unknown Error")
                return None
            return AnalyzeResults.from_json(rjson)

    def get_perspective_langs(self, ctx: typing.Union[commands.Context, FakeContext, discord.Message]):
        return [l for x in self.bot.i18n.get_langs(ctx)
                if (l := x.split('_')[0]) in self.perspective_langs]

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        # ignore empty messages
        if not message.content:
            return
        # ignore system messages
        if message.type != discord.MessageType.default and message.type != discord.MessageType.reply:
            return
        # ignore commands
        ctx: commands.Context = await self.bot.get_context(message)
        if ctx.valid:
            return
        # ignore DMs
        if isinstance(message.channel, discord.DMChannel) \
                or not isinstance(message.author, discord.Member):
            return
        # ignore self
        if message.author.id == self.bot.user.id:
            return
        # ignore users who are allowed to ban people
        if message.author.guild_permissions.ban_members:
            return

        # return values of True indicate that the message was deleted and does not need to be
        # further passed along
        if await self.pingban_handler(message):
            return

        if (modmail := self.bot.get_managed_channel(message.guild, "modmail")) is None:
            # don't execute handlers that require modmail
            return

        perms: discord.Permissions = message.guild.me.guild_permissions
        if not (perms.manage_messages and perms.manage_roles):
            return

        if await self.spam_handler(message, modmail):
            return
        if await self.perspective_handler(message, modmail):  # this should be last due to auto reports
            return

    async def pingban_handler(self, message: discord.Message) -> bool:
        # fail-safe: cancel if no ban perms
        # TODO: notify in modmail channel (needs a cooldown to not spam staff)
        if not message.guild.me.guild_permissions.ban_members:
            return False

        threshold = self.bot.config_get(message.guild.id, "ping_ban_threshold", default=0)
        if threshold <= 0 or (len(message.mentions) + len(message.role_mentions)) < threshold:
            return False

        join_thresh = self.bot.config_get(message.guild.id, "ping_ban_join_bypass", default=3)
        create_thresh = self.bot.config_get(message.guild.id, "ping_ban_creation_bypass", default=7)
        join_thresh_td = datetime.timedelta(days=join_thresh)
        create_thresh_td = datetime.timedelta(days=create_thresh)
        now = utcnow()
        join_diff = now - message.author.joined_at
        create_diff = now - message.author.created_at
        if (join_thresh == 0 or join_diff > join_thresh_td) and \
                (create_thresh == 0 or create_diff > create_thresh_td):
            return False

        ctx = FakeContext(bot=self.bot, guild=message.guild, channel=message.channel)
        await message.author.ban(reason=self.bot.i18n.localize("pingban_reason", ctx=ctx)
                                 .format(threshold))
        await message.channel.send(self.bot.i18n.localize("pingban_message", ctx=ctx))

    async def perspective_handler(self, message: discord.Message, modmail: discord.TextChannel) -> bool:
        if self.gtoken is None:
            return False
        if message.guild.id not in self.bot.bot_config['perspective']['whitelist']:
            return False
        context: typing.List[discord.Message] = []
        base = message
        while (ref := base.reference) is not None and (ctx := ref.cached_message) is not None:
            context.insert(0, ctx)  # perspective currently does not document the order of this parameter so im guessing
            base = ctx
        query = await self.analyze_comment(message.content, self.get_perspective_langs(message), message.guild.id,
                                           context)
        if query is None:
            return False
        if query[ScoreType.SEVERE_TOXICITY] >= 90 and (score := query[ScoreType.TOXICITY]) >= 99:
            if not await self.threshold_handler(CacheKey(message.author.id, message.guild.id),
                                                self.perspective_cache, message, modmail, 3):
                await self.send_report(message,
                                       self.bot.i18n.localize("perspective_report", guild=message.guild).format(score),
                                       modmail, message.guild.me, False)
            return True  # there is no appropriate return value here
        return False

    async def spam_handler(self, message: discord.Message, modmail: discord.TextChannel) -> bool:
        return await self.threshold_handler(SpamKey(message.author.id, message.guild.id, message.content),
                                            self.spam_cache, message, modmail)

    async def threshold_handler(self, key: CacheKey, cache: typing.Dict[CacheKey, CacheValue],
                                message: discord.Message, modmail: discord.TextChannel,
                                threshold: int = None):
        if threshold is None:
            threshold = self.bot.config_get(message.guild.id, "misbehavior_threshold", default=0)
        if threshold <= 0:
            return False
        logs: discord.TextChannel = self.bot.get_managed_channel(message.guild, "messages")
        if logs is None:
            return False  # TODO: configuration error alert (semicolon v2?)
        mute: discord.Role = message.guild.get_role(self.bot.config_get(message.guild.id, "muted_role"))
        if mute is None or mute >= message.guild.me.top_role:
            return False  # TODO: configuration error alert (semicolon v2?)

        if key in cache:
            value = cache[key]
            if value.lock.locked():
                return False
            async with value.lock:
                value.updated = utcnow()
                value.messages[message.id] = message
                if len(value.messages) >= threshold:
                    await message.author.add_roles(mute, reason=self.bot.i18n.localize("spam_reason", guild=mute.guild))
                    await modmail.send("@here: " + self.bot.i18n.localize("spam_alert", channel=modmail, guild=modmail.guild)
                                                                .format(message.author, logs, mute),
                                       allowed_mentions=discord.AllowedMentions(everyone=True, users=[message.author]))
                    for msg in value.messages.values():
                        # delete original message
                        try:
                            await msg.delete()
                        except (discord.Forbidden, discord.NotFound):
                            pass
                        # delete proxy message
                        pk_url = f"https://api.pluralkit.me/v2/messages/{msg.id}"
                        async with self.bot.session.request('GET', pk_url) as req:
                            if req.status == 200:
                                pk_body: dict[str, typing.Any] = await req.json()
                                if pk_body.get('original') == str(msg.id):
                                    try:
                                        await msg.channel.get_partial_message(int(pk_body['id'])).delete()
                                    except (discord.Forbidden, discord.NotFound):
                                        pass
                    del cache[key]
                    return True
        else:
            cache[key] = CacheValue(utcnow(), {message.id: message})
        return False

    @tasks.loop(seconds=5.0)
    async def cache_cleaner(self):
        self.clean_cache(self.spam_cache, self.spam_cache_expire)
        self.clean_cache(self.perspective_cache, self.persp_cache_expire)

    @staticmethod
    def clean_cache(cache: typing.Dict[CacheKey, CacheValue], expire: datetime.timedelta):
        for key, value in cache.copy().items():
            if (utcnow() - value.get_updated()) >= expire:
                del cache[key]

    async def consumer_handler(self, msg: typing.Dict[str, typing.Any]):
        if msg['action'] != "netban":
            return
        # TODO: save netban to DB?
        user_id: int = msg['user']
        guild_id: int = msg['guild']
        reason: typing.Optional[str] = msg['reason']
        guild: typing.Optional[guild_id] = self.bot.get_guild(guild_id)
        guild_name = guild.name if guild else str(guild_id)
        full_reason = f"Network ban via {guild_name}"
        public_reason = f"Network ban"  # TODO: i18n
        if reason:
            full_reason += f": {reason}"
            public_reason += f": {reason}"
        self.bot.logger.debug(str(user_id) + ": " + full_reason)

        htstem: discord.Guild = self.bot.get_guild(282219466589208576)
        await htstem.ban(discord.Object(user_id), reason=public_reason)

    async def add_join_role(self, member: discord.Member):
        """Adds the default role to a user"""
        guild: discord.Guild = member.guild
        ctx = FakeContext(bot=self.bot, guild=guild)

        try:
            # find if server has a join role
            role: discord.Role = await self.bot.get_variable(ctx, "join_role", False)
        except VariableError as ignored:
            return

        # add role
        if role and role not in member.roles and role < guild.me.top_role:
            await member.add_roles(role, reason=self.bot.i18n.localize("join_role_reason", guild=guild))

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        """Add role on joining server"""
        guild: discord.Guild = member.guild
        if guild.me.guild_permissions.ban_members and 'twitter.com/h0nde' in H0NDE.sub("", member.name.lower()):
            return await member.ban(reason="h0nde bot")

        # ignore users who have not consented to rules, or if self cannot manage roles
        if member.pending or not guild.me.guild_permissions.manage_roles:
            return
        await self.add_join_role(member)

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        """Add role on finishing reading rules"""
        guild: discord.Guild = after.guild

        # only get users who just finished reading rules
        if after.pending or not before.pending or not guild.me.guild_permissions.manage_roles:
            return
        await self.add_join_role(after)

    def get_rules_manager(self, guild):
        if isinstance(guild, discord.Guild):
            guild = guild.id
        return RuleManager(self.bot.sql, guild)

    async def channel_setup(self, ctx: commands.Context, constants_key: str, lang_key: str = "channel_created") -> str:
        cname = self.bot.bot_config['channels'][constants_key]
        existing_channel = discord.utils.get(ctx.guild.text_channels, name=cname)
        if existing_channel:
            return self.bot.i18n.localize('channel_fail', ctx=ctx).format(existing_channel)
        create_in = ctx.guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else ctx.guild
        overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(send_messages=False),
            ctx.guild.me: discord.PermissionOverwrite(send_messages=True, read_messages=True)
        }
        new_chan = await create_in.create_text_channel(cname, topic=self._(constants_key + '_topic', guild=ctx.guild),
                                                       overwrites=overwrites,
                                                       reason=self._('reason', guild=ctx.guild).format(ctx.author))
        self.bot.config_set(ctx.guild.id, constants_key+"_channel", new_chan.id)
        return self.bot.i18n.localize(lang_key, ctx=ctx).format(new_chan, ctx.prefix)

    def get_rules(self, guild: typing.Union[int, discord.Guild]) -> typing.List[typing.Tuple[str, bool]]:
        rules = self.get_rules_manager(guild).get_rows()
        if not rules:
            return []

        return [(x[0], bool(x[1])) for x in rules]

    async def process_report(self, reactpayload: discord.RawReactionActionEvent):
        gid: int = reactpayload.guild_id
        cid: int = reactpayload.channel_id
        mid: int = reactpayload.message_id
        emoji: discord.PartialEmoji = reactpayload.emoji
        reporter: discord.Member = reactpayload.member
        guild: discord.Guild = self.bot.get_guild(gid)
        channel: discord.TextChannel = guild.get_channel(cid)
        message: discord.Message = await channel.fetch_message(mid)
        author: discord.Member = message.author
        kwargs = {'guild': guild.id, 'channel': channel.id, 'user': reporter.id}
        _ = self.bot.i18n.localize

        if channel.permissions_for(guild.me).manage_messages:
            await message.remove_reaction(emoji, reporter)
        if author == reporter:
            return
        modmail: discord.TextChannel = self.bot.get_managed_channel(guild, "modmail")
        if modmail is None:
            return

        mperm: discord.Permissions = modmail.permissions_for(guild.me)
        if not (mperm.read_messages and mperm.send_messages
                and mperm.add_reactions and mperm.embed_links):
            # bot is missing permissions, but we should only report the error if the report emoji
            # is from the server (ie if the server has setup reporting)
            femoji: discord.Emoji
            if (femoji := self.bot.get_emoji(emoji.id)) and femoji.guild_id == gid:
                try:
                    owner: discord.Member = guild.owner
                    gname = discord.utils.escape_markdown(guild.name)
                    msg = _('report_owner_error', guild=guild.id, user=owner.id).format(gname, modmail.mention, emoji)
                    await owner.send(msg)
                except discord.Forbidden:
                    pass
            return

        raw_rules = self.get_rules(guild)

        ctx = FakeContext(bot=self.bot, guild=guild, channel=channel, author=reporter)
        rule_select = await prompt(
            ctx,
            _('report_dm_prompt', **kwargs).format(message.jump_url),
            menu_options=[MenuOptionArgs(str(i), rule) for i, (rule, _) in enumerate(raw_rules)] +
                [MenuOptionArgs('report_dm_other', _('report_dm_other', **kwargs))],
            button_options=[
                ButtonOptionArgs('report_dm_cancel', _('report_dm_action_cancel', **kwargs)),
                ButtonOptionArgs(None, _('report_dm_action_jump_to_message', **kwargs), url=message.jump_url)
            ],
            dest=reporter)

        if rule_select is None or rule_select == 'report_dm_cancel':
            return

        if rule_select == 'report_dm_other':
            modmail_rule_text = _('report_dm_other', channel=modmail, guild=guild)
            ping = False
        else:
            modmail_rule_text, ping = raw_rules[int(rule_select)]

        await self.send_report(message, modmail_rule_text, modmail, reporter, ping)

    async def send_report(self, message: discord.Message, rule: str,
                          modmail: discord.TextChannel = None,
                          reporter: typing.Union[discord.User, discord.Member, None] = None,
                          ping: bool = False):
        _ = self.bot.i18n.localize

        if modmail is None:
            modmail = self.bot.get_managed_channel(message.guild, "modmail")
        if modmail is None:
            return

        if reporter is None:
            reporter = self.bot.user

        mcontent = '@here' if ping else None
        mentions = discord.AllowedMentions.none()
        mentions.everyone = ping

        kwargs = {'guild': message.guild.id, 'channel': modmail.id}

        embed = discord.Embed(title=shorten(_('report_modmail_title', **kwargs)
                                            .format(message.channel), 256),
                              color=CONSTANTS['colors']['message_delete'],
                              description=_('report_modmail_desc', **kwargs)
                                            .format(message.jump_url),
                              timestamp=message.created_at)
        # display reporter
        embed.add_field(name=_('report_modmail_reporter_title', **kwargs),
                        value=_('report_modmail_user_desc', **kwargs).format(reporter),
                        inline=False)
        # display reported user
        embed.add_field(name=_('report_modmail_user_title', **kwargs),
                        value=_('report_modmail_user_desc', **kwargs).format(message.author),
                        inline=False)
        # display reported rule
        embed.add_field(name=_('report_modmail_rule_title', **kwargs), value=rule, inline=False)
        # get message contents
        if message.system_content:
            content = shorten(message.system_content, 1024)
        else:
            content = _('message_blank_content', channel=modmail, guild=message.guild)
        # display message content
        embed.add_field(name=_('report_modmail_post_title', **kwargs), value=content, inline=False)
        # set footer
        embed.set_footer(text=CONSTANTS['report_footer'],
                         icon_url=self.bot.user.display_avatar.replace(format='png', size=128).url)
        # add attached image
        embed = add_attachment_info(embed, message, self.bot.i18n)

        view = discord.ui.View(timeout=None)
        for action in QuickActions:
            view.add_item(discord.ui.Button(
                custom_id=action.name,
                label=_(action.value.button_label, **kwargs),
                style=action.value.button_style))

        await modmail.send(mcontent, embed=embed, allowed_mentions=mentions, view=view)

    async def quick_action(self, interaction: discord.Interaction):
        moderator: discord.Member = interaction.user
        guild = interaction.guild
        channel = interaction.channel
        message = interaction.message

        if not channel or not message or not guild:
            return

        # check action
        if interaction.data is None or\
            interaction.data.get('custom_id', None) not in QuickActions.__members__:
            return
        action: str = interaction.data['custom_id']

        if channel != self.bot.get_managed_channel(guild, 'modmail'):
            return  # not the modmail channel
        author: discord.Member = message.author
        kwargs = {'guild': guild.id, 'channel': channel.id, 'user': moderator.id}
        _ = self.bot.i18n.localize
        _p = self.bot.i18n.localize_p

        # ensure reacted message is a modmail message
        if author.id != self.bot.user.id:
            return  # not my post, ignore it
        if not message.embeds:
            return
        embed = message.embeds[0]
        if embed.footer is None:
            return
        if embed.footer.text != CONSTANTS['report_footer']:
            return
        if embed.description is None or (match := self.report_match.search(embed.description)) is None:
            return
        r_cid, r_mid = map(int, match.group(2, 3))

        # get info of reported post
        r_chan: discord.TextChannel = guild.get_channel(r_cid)

        # message & author
        r_auth_id = int(self.id_match.search(embed.fields[1].value).group(1))
        try:
            r_msg = await r_chan.fetch_message(r_mid)
            r_author = r_msg.author
            r_auth_id = r_author.id
        except discord.NotFound:
            r_msg = None
            r_author = guild.get_member(r_auth_id)

        uperms: discord.Permissions = r_chan.permissions_for(moderator)
        perms: discord.Permissions = r_chan.permissions_for(guild.me)
        # run action
        reason = _('report_action_reason', guild=guild).format(moderator)
        if action == 'DISMISS':
            await clear_buttons(interaction)
        elif action == 'DELETE':
            if uperms.manage_messages and perms.manage_messages:
                if r_msg:
                    await r_msg.delete()
                await clear_buttons(interaction, 'DELETE')
        elif action == 'BAN':
            if uperms.ban_members and perms.ban_members:
                days = list(range(1, 8))
                ctx = FakeContext(self.bot, guild, channel, moderator)
                del_days_str = await prompt(
                    ctx,
                    _('report_ban_desc', **kwargs),
                    menu_options=[MenuOptionArgs(str(day), _p('report_ban_days', day, **kwargs).format(day)) for day in days],
                    button_options=[ButtonOptionArgs('cancel', _('cancel_action', **kwargs), style=discord.ButtonStyle.secondary)]
                )
                if del_days_str is not None and del_days_str != 'cancel':
                    if r_auth_id:
                        await guild.ban(discord.Object(r_auth_id), reason=reason, delete_message_days=int(del_days_str))
                    try:
                        await clear_buttons(interaction, 'BAN')
                    except discord.errors.NotFound:
                        # original message deleted by prompt; no buttons to remove
                        pass
        elif action == 'KICK':
            if uperms.kick_members and perms.kick_members:
                if r_author:
                    await r_author.kick(reason=reason)
                await clear_buttons(interaction, 'KICK')
        else:
            raise NotImplementedError("Reached invalid state with action {}".format(action))

    @commands.Cog.listener()
    async def on_interaction(self: 'Moderation', interaction: discord.Interaction):
        if interaction.data is not None and 'custom_id' in interaction.data:
            if interaction.data['custom_id'] in QuickActions.__members__:
                return await self.quick_action(interaction)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self: 'Moderation', reactpayload: discord.RawReactionActionEvent):
        _ = self.bot.i18n.localize
        # ignore DM reactions
        if not (gid := reactpayload.guild_id):
            return
        emoji: discord.PartialEmoji = reactpayload.emoji
        member: discord.Member = reactpayload.member
        if member is None:
            return
        if member.id == self.bot.user.id:  # ignore self obv
            return

        guild: discord.Guild = self.bot.get_guild(gid)
        channel: 'discord.abc.MessageableChannel' = self.bot.get_channel(reactpayload.channel_id)
        if not channel:
            return
        if emoji.is_custom_emoji():
            variable = self.bot.config_get(guild.id, "report_emoji")
            if (variable is None and emoji.name == _('report_emoji', guild=guild))\
                    or (variable is not None and variable == emoji.id):
                return await self.process_report(reactpayload)

    @commands.group(invoke_without_command=True, aliases=['rule'])
    @commands.guild_only()
    async def rules(self, ctx: commands.Context, rule_number: int = None):
        """
        Lists the servers rules.
        Moderators can add rules to be displayed when reporting a message (see `;help setup reports`)
        """
        rules = self.get_rules(ctx.guild)
        if not rules:
            await ctx.send(self._('rules_none', ctx=ctx))
            return

        if rule_number:
            if len(rules) <= rule_number:
                out = self._('rules_get', ctx=ctx).format(ctx.author.mention, rules[rule_number-1])
            else:
                out = self._('rules_missing', ctx=ctx).format(ctx.author.mention, rule_number)
            await ctx.send(out)
            return

        rules = [self._('rules_list', ctx=ctx).format(c, rule[0]) for c, rule in enumerate(rules, 1)]
        rules.append(self._('rules_requested', ctx=ctx).format(ctx.author.mention))
        for m in line_split('\n'.join(rules)):
            await ctx.send(m)

    @rules.command(name='add', aliases=['create'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_add(self, ctx: commands.Context, *, rule: str):
        """
        Adds a new rule to the server.
        """
        self.get_rules_manager(ctx.guild).create_row((rule, int(False)))
        await ctx.send(self._('rules_add', ctx=ctx).format(ctx.author.mention))

    @rules.command(name='set', aliases=['modify', 'update'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_set(self, ctx: commands.Context, rule_number: int, *, rule: str):
        """
        Modifies an existing server rule.
        """
        try:
            key = 'success'
            self.get_rules_manager(ctx.guild).edit_row(rule_number, (rule,), ['rule'])
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_set_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @rules.command(name='remove', aliases=['delete'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_remove(self, ctx: commands.Context, rule_number: int):
        """
        Removes a server rule.
        """
        try:
            key = 'success'
            self.get_rules_manager(ctx.guild).delete_row(rule_number)
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_rem_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @rules.group(name='ping', aliases=['alert'], invoke_without_command=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_ping(self, ctx: commands.Context, rule_number: int, toggle: bool = None):
        """
        Toggles if a rule should ping @here when reported.
        """
        try:
            if toggle is None:
                toggle = not(bool(self.get_rules(ctx.guild)[rule_number-1][1]))
            key = f'success_{str(toggle)}'
            self.get_rules_manager(ctx.guild).edit_row(rule_number, (int(toggle),), ['ping_here'])
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_ping_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @r_ping.command(name='list', invoke_without_command=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def rp_list(self, ctx: commands.Context):
        """
        Lists rules that will ping @here when reported.
        """
        rules = self.get_rules(ctx.guild)
        rules = [self._('rules_list', ctx=ctx).format(c, rule[0]) for c, rule in enumerate(rules, 1) if rule[1]]
        if not rules:
            await ctx.send(self._('rules_ping_none', ctx=ctx).format(ctx.author.mention))
            return
        rules.append(self._('rules_requested_short', ctx=ctx).format(ctx.author.mention))
        for m in line_split('\n'.join(rules)):
            await ctx.send(m)

    @rules.command(name='import')
    @commands.guild_only()
    @commands.is_owner()
    async def r_import(self, ctx: commands.Context):
        cols = ["rule text NOT NULL", "ping_here integer NOT NULL"]
        temp = TableManager(self.bot.sql, f'rules_{ctx.guild.id}', cols)
        for row in temp.get_rows():
            self.get_rules_manager(ctx.guild).create_row(row)
        temp.delete()

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def setup(self, ctx: commands.Context):
        """Sets up a semicolon feature on a server."""
        await ctx.send_help(ctx.command)

    @setup.command(name='joinlog', brief='Creates a channel that logs joins, leaves, & name changes.')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_joinlog(self, ctx: commands.Context):
        """Creates a channel that logs member joins, leaves, bans, unbans, name changes, and discriminator changes."""
        await ctx.send(await self.channel_setup(ctx, 'joins'))

    @setup.command(name='messagelog')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_messages(self, ctx: commands.Context):
        """Creates a channel that logs edited and deleted messages."""
        await ctx.send(await self.channel_setup(ctx, 'messages'))

    @setup.command(name='rolelog')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_roles(self, ctx: commands.Context):
        """Creates a channel that logs added and removed roles from users."""
        await ctx.send(await self.channel_setup(ctx, 'roles'))

    @setup.command(name='starboard')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_starboard(self, ctx: commands.Context):
        """Creates a channel that highlights messages "starred" by many users."""
        await ctx.send(await self.channel_setup(ctx, 'starboard', 'starboard_created'))

    @setup.command(name='pronouns')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def setup_pronouns(self, ctx: commands.Context):
        """Sets up automatic, cross-server pronoun roles."""
        toprole = ctx.guild.me.top_role
        for pronoun in CONSTANTS['pronoun_whitelist']:
            p_name = self.bot.bot_config['pronoun_prefix'] + pronoun
            p_role = discord.utils.find(lambda r: r.name == p_name and toprole > r, ctx.guild.roles)
            if not p_role:
                await ctx.guild.create_role(name=self.bot.bot_config['pronoun_prefix'] + pronoun)
                # pronoun cog will detect this role creation and create the rest. so, to avoid race conditions,
                break
        await asyncio.sleep(.1)
        await ctx.send(self.bot.i18n.localize('pronouns_created', ctx=ctx))

    @setup.command(name='reports', aliases=['report', 'reporting'])
    @commands.has_guild_permissions(manage_channels=True, manage_emojis=True)
    @commands.bot_has_guild_permissions(manage_channels=True, manage_emojis=True, manage_messages=True)
    async def setup_reports(self, ctx: commands.Context):
        """
        Adds an emoji for users to report posts and a channel to log the reports.
        Upon seeing a rule-breaking post, users can react with the added emoji and it will be sent to the modmail channel for moderators to take action.
        """
        # short vars
        guild: discord.Guild = ctx.guild
        me: discord.Member = guild.me
        _ = self.bot.i18n.localize

        # restrict report access to only users with the join role
        roles = []
        try:
            join_role: discord.Role = await self.bot.get_variable(ctx, "join_role", False)
            if join_role is not None:
                roles.append(join_role)
        except VariableError as ignored:
            pass

        # create emoji
        ename = _('report_emoji', guild=guild)  # emoji name
        if (emoji := discord.utils.get(guild.emojis, name=ename)) is None:  # if emoji doesn't already exist
            try:
                with open(CONSTANTS['report_emoji_path'], 'rb') as f:
                    emoji = await guild.create_custom_emoji(name=ename, image=f.read(), roles=roles)
                    self.bot.config_set(guild.id, 'report_emoji', emoji.id)
            except discord.HTTPException as e:
                raise ExpectedError(str(e))

        # create text channel
        cname = self.bot.bot_config['channels']['modmail'].lower()  # channel name
        if (channel := discord.utils.get(guild.text_channels, name=cname)) is None:  # if channel doesn't already exist
            dest = guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else guild
            if not dest:
                dest = guild

            # set basic text channel overwrites
            overwrites = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(manage_messages=True, read_messages=True,
                                                      use_external_emojis=True,
                                                      add_reactions=True, read_message_history=True)
            }

            # allow mods to access channel
            for join_role in guild.roles:
                perms = join_role.permissions
                if (perms.kick_members and perms.manage_messages) or perms.administrator:
                    overwrites[join_role] = discord.PermissionOverwrite(read_messages=True)

            # attempt creation
            try:
                channel = await dest.create_text_channel(cname, overwrites=overwrites,
                                                         topic=_('modmail_topic', guild=guild))
            except discord.HTTPException as e:
                raise ExpectedError(str(e))

        # generate output
        out = [_('report_setup_out', ctx=ctx).format(emoji, channel.mention, ctx.prefix)]
        # warn of channels semicolon can't manage
        chans = [x.mention for x in guild.text_channels if not x.permissions_for(me).manage_messages and x.permissions_for(me).read_messages]
        # warn of missing guild permissons
        perms: discord.Permissions = guild.me.guild_permissions
        kick = [_('report_setup_out_kick', ctx=ctx)] if not perms.kick_members else []
        ban = [_('report_setup_out_ban', ctx=ctx)] if not perms.ban_members else []
        # add outputs
        if chans or kick or ban:
            out.append(_('report_setup_out_consider', ctx=ctx))
            if chans:
                out.append(_('report_setup_out_channels', ctx=ctx).format(comma_separator(chans)))
            out += kick
            out += ban

        # send output
        await ctx.send('\n'.join(out))

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def toggle(self, ctx: commands.Context, command: str, action: bool):
        """
        Enables or disables a command for the current guild.
        """
        cmd = ctx.bot.get_command(command)

        if cmd is None:
            await ctx.send(self.bot.i18n.localize('toggle_no_command', ctx=ctx))
            return

        while cmd.parent is not None:  # get the base command
            cmd = cmd.parent

        if cmd.name in ['toggle']:
            await ctx.send(self.bot.i18n.localize('toggle_no_toggle_toggle', ctx=ctx))
            return

        bot: 'Semicolon' = ctx.bot
        manager = bot.get_disabled_cmds_manager(ctx.guild.id)

        rows = manager.get_rows_by({'command_name': cmd.name})
        already_disabled = len(rows) > 0

        if not action:
            if not already_disabled:
                manager.create_row((cmd.name, ))
        else:
            if already_disabled:
                manager.delete_row_by({'command_name': cmd.name})

        langkey = '_'.join([
            'toggle',
            # action is true to enable, false to disable
            'success' if action == already_disabled else 'failure',
            'enable' if action else 'disable'
        ])
        await ctx.send(self.bot.i18n.localize(langkey, ctx=ctx).format(ctx.prefix, cmd.name, ctx.guild.name))

    @commands.command()
    @commands.is_owner()
    async def toggle_legacy_import(self, ctx: commands.Context):
        """Imports disabled commands from the legacy database. This may overwrite existing data!"""
        disabled_cmds = data_path('disabled_commands.json')
        if not os.path.exists(disabled_cmds):
            return
        with open(disabled_cmds, 'r') as f:
            data = json.load(f)
            for guild, commands in data.items():
                manager = ctx.bot.get_disabled_cmds_manager(int(guild))
                for command_name in commands:
                    manager.create_row((command_name, ))
        await ctx.send(get_constant_emoji('SUCCESS', ctx.channel))


    @commands.group(aliases=['purge', 'delete'], invoke_without_command=True)
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def clear(self, ctx, messages: int):
        """
        Deletes X amount of messages.
        X does not include the message invoking the command.
        """
        warning_limit = CONSTANTS['clear_count_warning_threshold']

        if messages <= 0:
            raise UserInputError(self.bot.i18n.localize('clear_count_too_low', ctx=ctx))

        messages = messages+1
        if messages <= warning_limit or await prompt(
            ctx,
            self.bot.i18n.localize_p('clear_large_count_warning', warning_limit, ctx=ctx).format(warning_limit),
            button_options=[
                ButtonOptionArgs('clear', self.bot.i18n.localize('clear_messages_action', ctx=ctx), discord.ButtonStyle.danger),
                ButtonOptionArgs('cancel', self.bot.i18n.localize('cancel_action', ctx=ctx), discord.ButtonStyle.secondary),
            ]
        ) == 'clear':
            await ctx.channel.purge(limit=messages)

    @clear.command(name='after')
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def clear_after(self, ctx: commands.Context, message_id: int, upto_id: int = None):
        """
        Clears messages sent after a provided message ID.
        The provided message ID is *exclusive*, the optional upto ID is *inclusive*.
        Command works by extracting the time from the ID and deleting anything afterwards, so any ID is valid input.
        Will ask for confirmation if you try to clear over 1 days worth of messages.
        """
        dtstop = discord.utils.snowflake_time(message_id)  # gets datetime object corresponding to the input
        dtstrt = None
        if upto_id is not None:
            dtstrt = discord.utils.snowflake_time(upto_id) - datetime.timedelta(seconds=.001)

        async def the_purge():
            await ctx.channel.purge(after=dtstop, before=dtstrt, limit=None)
            if dtstrt and dtstrt <= ctx.message.created_at:
                try:
                    await ctx.message.delete()
                except (discord.HTTPException, discord.Forbidden):
                    pass

        if ((utcnow() - dtstop) <= datetime.timedelta(days=1)) or await prompt(
            ctx,
            self.bot.i18n.localize('clear_after_warning', ctx=ctx),
            button_options=[
                ButtonOptionArgs('clear', self.bot.i18n.localize('clear_messages_action', ctx=ctx), discord.ButtonStyle.danger),
                ButtonOptionArgs('cancel', self.bot.i18n.localize('cancel_action', ctx=ctx), discord.ButtonStyle.secondary),
            ]
        ) == 'clear':
            await the_purge()

    @commands.command(aliases=['hackban'])
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    @commands.guild_only()
    async def ban(self, ctx: commands.Context, user: SnowflakeConvert(MEMBER),
                                               days_to_delete: typing.Optional[int] = 0,
                                               *reason: str):
        """(Hack)bans a user from a server. Accepts IDs and fuzzy search."""
        user: discord.Member
        if user.id == ctx.guild.owner_id:
            raise UserInputError(self.bot.i18n.localize("ban_owner", ctx=ctx))
        if hasattr(user, 'top_role') and user.top_role >= ctx.me.top_role:
            raise HierarchyError(self.bot.i18n.localize("ban_hierarchy", ctx=ctx))
        if not (0 <= days_to_delete <= 7):
            raise UserInputError(self.bot.i18n.localize("invalid_range", ctx=ctx)
                                 .format(days_to_delete, "days_to_delete", 0, 7))
        guild: discord.Guild = ctx.guild
        reason_s = ' '.join(reason)
        fmt = (f"{str(ctx.author)} ({ctx.author.id})", ctx.prefix, ctx.invoked_with)
        reason_s += self.bot.i18n.localize("hackban_reason", guild=guild).format(*fmt)
        reason_s = shorten(reason_s.strip(), 512)
        await guild.ban(user, reason=reason_s, delete_message_days=days_to_delete)
        await ctx.send(self.bot.i18n.localize("hackban", ctx=ctx).format(user.id))

    @commands.group(invoke_without_command=True)
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config(self, ctx: commands.Context):
        """Change guild variables."""
        await ctx.send_help(ctx.command)

    @staticmethod
    def option_type(ctx: commands.Context, value, key: str):
        if isinstance(value, BaseConverter):
            text = value.get_i18n_name(ctx)
        else:
            text = value.__name__
        text = f"({text})"
        key = 'note_'+key
        if (note := ctx.bot.i18n.localize(key, ctx=ctx)) != f"[{key}]":
            text += f"\n  *\\* {note}*"
        return text

    @config.command(name="list")
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config_list(self, ctx: commands.Context):
        """Lists all available config options."""
        opts = [f"- `{k}` {self.option_type(ctx, v, k)}" for k, v in self.bot.config_options.items()]
        opts.insert(0, self.bot.i18n.localize("config_list", ctx=ctx))
        await ctx.send("\n".join(opts))

    @config.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def get(self, ctx: commands.Context, variable: str):
        """Gets the current value of a variable."""
        p_data = display_variable(await self.bot.get_variable(ctx, variable))
        await ctx.send(self.bot.i18n.localize("get_variable", ctx=ctx).format(variable, p_data))

    @config.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def set(self, ctx: commands.Context, variable: str, *, data: str):
        """Sets the value of a variable."""
        variable = self.bot.validate_variable(variable)
        try:
            p_data = display_variable(await self.bot.set_variable(ctx, variable, data))
            await ctx.send(self.bot.i18n.localize("set_variable", ctx=ctx).format(variable, p_data))
        except VariableParseError:
            await ctx.send(self.bot.i18n.localize("set_variable_parse_error", ctx=ctx))

    @config.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def append(self, ctx: commands.Context, variable: str, *, data: str):
        """Adds elements to a List config variable."""
        variable = self.bot.validate_variable(variable)
        if isinstance(self.bot.config_options[variable], List):
            curr = await self.bot.get_variable(ctx, variable)
            new = await self.bot.interpret_data(ctx, variable, data)
            data = list(set(new + curr))
            p_data = display_variable(await self.bot.set_variable(ctx, variable, data))
            await ctx.send(self.bot.i18n.localize("set_variable", ctx=ctx).format(variable, p_data))
        else:
            raise VariableTypeError(0, variable, "List")

    @config.command(name='clear')
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config_clear(self, ctx: commands.Context, variable: str):
        """Clears (removes) the value of a variable."""
        variable = self.bot.validate_variable(variable)
        self.bot.config_clear(ctx.guild.id, variable)
        await ctx.send(self.bot.i18n.localize("clear_variable", ctx=ctx).format(variable))

    @commands.command()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def mute(self, ctx: commands.Context, member: SnowflakeConvert(MEMBER, allow_partial=False), *, time: TimeConvert):
        """Mutes a user for a specified duration."""
        member: discord.Member
        time: ConvertedTime
        try:
            mute_role: discord.Role = await self.bot.get_variable(ctx, "muted_role")
            if mute_role >= ctx.guild.me.top_role:
                await ctx.send(self.bot.i18n.localize("mute_too_high", ctx=ctx))
                return
        except VariableMissing:
            await ctx.send(self.bot.i18n.localize("no_mute_role", ctx=ctx))
            return

        args = (member.id, ctx.guild.id, mute_role.id)
        self.bot.add_task(time.time, unmute, *args)

        human_time = humanize.naturaldelta(time.time, when=time.when)
        if mute_role not in member.roles:
            reason = self.bot.i18n.localize("mute_reason", guild=ctx.guild).format(human_time, ctx.author)
            await member.add_roles(mute_role, reason=reason)
        await ctx.send(self.bot.i18n.localize("muted", ctx=ctx).format(member, human_time))


async def setup(bot):
    await bot.add_cog(Moderation(bot))
