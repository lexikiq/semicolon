import asyncio
import json
import logging
import math
import os
import typing
from collections.abc import MutableMapping

import discord
from babel.plural import to_python
from discord.ext import commands
from discord.mixins import Hashable
from discord.utils import escape_markdown as esc_md
from yaml import load

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

# noinspection PyPackages -- not applicable
from .constants import CONSTANTS
# noinspection PyPackages -- not applicable
from .errors import GuildExclusiveCommand, ExpectedError


def is_stys(ctx, raise_error: bool = True):
    return guild_check(ctx, CONSTANTS['stys_id'], raise_error=raise_error)


class JsonManager:
    __slots__ = {"data", "_file"}

    def __init__(self, filename):
        self.data = {}
        self._file = os.path.join(CONSTANTS['data_folder'], filename)
        if not os.path.exists(self._file):
            self.save()
        else:
            self._load()

    def _load(self):
        self.data = json_load(self._file)

    def save(self):
        json_save(self._file, self.data)

    def init_data(self, key: str, init_with=None):
        """Creates objects in json dict if non-existent"""
        if init_with is None:
            init_with = {}
        if key not in self.data:
            self.data[key] = init_with


class Localizer:
    __slots__ = {"config", "_config_file", "langs", "keys", "_default_lang_key"}
    with open(os.path.join('cldr', 'plurals.json'), 'r') as f:
        _plural_rules = {x: to_python({w.replace('pluralRule-count-', ''): z for w, z in y.items()})
                            for x, y in json.load(f)['supplemental']['plurals-type-cardinal'].items()}

    def __init__(self):
        self.config = {}
        self._config_file = os.path.join(CONSTANTS['data_folder'], "lang_select.json")
        if not os.path.exists(self._config_file):
            self.save_config()
        self._load_config()

        self.langs = {}
        self.keys = []
        self.load_langs()
        self._default_lang_key = 'en'
        if self._default_lang_key not in self.langs:
            raise FileNotFoundError("Default language file not found! All messages will be missing!")

    def clear_removed_langs(self):
        for snowflake, lang in self.config.copy().items():
            if lang not in self.langs:
                del self.config[snowflake]
        self.save_config()

    def load_langs(self):
        path = CONSTANTS['lang_folder']
        langs = []
        for file in os.listdir(path):
            if file.startswith('_'):
                continue
            lname = file.split('.')[0]
            langs.append(lname)
            with open(os.path.join(path, file), 'r', encoding='UTF-8') as f:
                _d = load(f, Loader)
                if lname in _d:
                    _d = _d[lname]
                if "_NAME_" not in _d:
                    continue
                self.langs[lname] = _d
        # if a language is for some reason removed, remove it !
        for lname in self.langs.copy().keys():  # idk if i need to copy?? but just in case??
            if lname not in langs:
                del self.langs[lname]
        self.clear_removed_langs()

        # create cache of keys, for.. various purposes?
        self.keys = []
        for d in self.langs.values():
            for key in d.keys():
                # if key has not been added and is not a comment
                if key not in self.keys and not key.startswith('_'):
                    self.keys.append(key)

    def localize(self, key: str, lang: str = None, ctx: commands.Context = None,
                 user: typing.Union[int, Hashable] = None,
                 channel: typing.Union[int, Hashable] = None,
                 guild: typing.Union[int, Hashable] = None,
                 default="[{}]",
                 suppress_kwarg_warn=False) -> str:

        if not (lang or user or channel or guild or ctx or suppress_kwarg_warn):
            logging.getLogger('bot').debug(f"`localize` method was called without kwargs for {key}")

        for _lang in [lang] + self._get_raw_langs(ctx, user, channel, guild):
            if _lang is not None and (text := self._get_text(key, _lang)) is not None:
                return text

        # PANIC!! text not found anywhere!!
        if isinstance(default, str):
            return default.format(key)
        return default

    def localize_p(self, key: str, count: int, lang: str = None, ctx: commands.Context = None, user: int = None,
                   channel: typing.Union[int, Hashable] = None, guild: typing.Union[int, Hashable] = None, default="[{}]", suppress_kwarg_warn=False) -> str:
        if not (lang or user or channel or guild or ctx or suppress_kwarg_warn):
            logging.getLogger('bot').debug(f"`localize` method was called without kwargs for {key}")

        texts = None
        _lang = lang  # make IDE shut up about reference before assignment
        for _lang in [lang] + self._get_raw_langs(ctx, user, channel, guild):
            if _lang is not None and (texts := self._get_text(key, _lang)) is not None:
                break

        if texts is None:
            if isinstance(default, str):
                return default.format(key)
            return default

        if _lang is None or _lang not in self._plural_rules:
            _lang = self._default_lang_key
        return texts[self._plural_rules[_lang.split('_')[0]](count)]

    def get_langs(self, ctx: commands.Context = None,
                  user: typing.Union[int, Hashable] = None,
                  channel: typing.Union[int, Hashable] = None,
                  guild: typing.Union[int, Hashable] = None) -> typing.List[str]:
        return [x for x in self._get_raw_langs(ctx, user, channel, guild) if x is not None]

    def _get_raw_langs(self, ctx: commands.Context = None,
                       user: typing.Union[int, Hashable] = None,
                       channel: typing.Union[int, Hashable] = None,
                       guild: typing.Union[int, Hashable] = None) -> typing.List[str]:
        # stupid ass hell of validating my shitty ass inputs
        if user:
            if isinstance(user, str):
                user = int(user)
            elif not isinstance(user, int):
                user = user.id

        if guild:
            if isinstance(guild, str):
                guild = int(guild)
            elif not isinstance(guild, int):
                guild = guild.id

        if channel:
            if isinstance(channel, str):
                channel = int(channel)
            elif not isinstance(channel, int):
                channel = channel.id

        if ctx:
            if ctx.author and user is None:
                user = ctx.author.id
            if ctx.channel and channel is None:
                channel = ctx.channel.id
            if ctx.guild and guild is None:
                guild = ctx.guild.id

        return [self._get_lang_by_id(user),
                self._get_lang_by_id(channel, is_channel=True),
                self._get_lang_by_id(guild, is_guild=True),
                self._default_lang_key]

    def _get_lang_by_id(self, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> typing.Optional[str]:
        if snowflake is None:
            return None
        return self.get_config(snowflake, is_channel=is_channel, is_guild=is_guild)

    def _get_text_by_id(self, key: str, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> typing.Union[str, None]:
        lang = self.get_config(snowflake, is_channel=is_channel, is_guild=is_guild)
        if lang is not None:
            return self._get_text(key, lang)
        return None

    def _get_text(self, key: str, lang: str) -> typing.Union[str, None]:
        if lang in self.langs:
            ldict = self.langs[lang]
            if key in ldict:
                return ldict[key]
        return None

    def _load_config(self):
        self.config = json_load(self._config_file)

    def save_config(self):
        json_save(self._config_file, self.config)

    def _get_config_key(self, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> str:
        key = str(snowflake)
        # old channels and guild can have same ID, so ya gotta differentiate
        if is_channel and is_guild:
            raise Exception("Invalid config key")
        if is_channel:
            key += 'c'
        if is_guild:
            key += 'g'
        return key

    def write_config(self, lang: str, snowflake: int, is_channel: bool = False, is_guild: bool = False):
        if lang not in list(self.langs.keys())+['_']:
            raise FileNotFoundError("Language doesn't exist")
        key = self._get_config_key(snowflake, is_channel, is_guild)
        if lang == '_':
            if key in self.config:
                del self.config[key]
        else:
            self.config[key] = lang
        self.save_config()

    def get_config(self, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> typing.Union[str, None]:
        key = self._get_config_key(snowflake, is_channel, is_guild)
        if key in self.config:
            return self.config[key]
        return None


def json_load(filename: str):
    """Loads JSON file"""
    with open(filename, 'r') as f:
        return json.load(f)


def json_save(filename: str, cache: dict):
    """Saves a dict to minified json file"""
    with open(filename, 'w') as f:
        json.dump(cache, f, separators=(',', ':'))


def line_split(input_message, char_limit=2000):
    output = []
    for line in input_message.split('\n'):
        line = line.strip() + '\n'
        if output and len((output[-1] + line).strip()) <= char_limit:
            output[-1] += line
        else:
            while line:
                output.append(line[:char_limit])
                line = line[char_limit:]
    return [msg.strip() for msg in output]


def guild_check(ctx: commands.Context, guild_id: int, no_name: bool = False, raise_error: bool = True) -> bool:
    """
    Checks if the context matches an input guild ID, and raises a descriptive error if not.
    :param ctx: context from a command
    :param guild_id: guild id to check against
    :param no_name: (optional) hides the expected server name
    :param raise_error: (optional) raises a custom error message
    :return: True or raises a GuildExclusiveCommand error (or False)
    """
    if ctx.guild and ctx.guild.id == guild_id:
        return True
    if no_name:
        guild = ctx.bot.i18n.localize('secret_guild', ctx=ctx)
    else:
        guild = ctx.bot.get_guild(guild_id)
        guild = guild_id if guild is None else guild
    if raise_error:
        raise GuildExclusiveCommand(ctx.bot.i18n.localize('guild_exclusive', ctx=ctx).format(guild))
    else:
        return False


def get_constant_emoji(
    emoji: str,
    channel: 'discord.abc.MessageableChannel'):
    if emoji not in CONSTANTS['emojis']:
        err = "Emoji \"{}\" not found"
        raise ValueError(err.format(emoji))
    emojis = CONSTANTS['emojis'][emoji]

    # Only one emoji, no custom/fallback.
    if isinstance(emojis, str):
        return emojis

    # DM channel. We can use external emojis.
    if channel.guild is None:
        return emojis[0]

    return emojis[not channel.permissions_for(channel.guild.me).use_external_emojis]


def is_chip(ctx: commands.Context) -> bool:
    """Returns bool of if current guild is ChiP or not. Hides the name because it's funny."""
    return guild_check(ctx, 381941901462470658, no_name=True)


def embed_author_template(ctx: commands.Context, subtle_author: typing.Union[bool, None] = False) -> discord.Embed:
    """Creates an embed template"""
    embed = discord.Embed(timestamp=ctx.message.created_at, color=CONSTANTS['colors']['default'])
    if subtle_author is not None:
        if not subtle_author:
            if isinstance(ctx.channel, discord.TextChannel):
                embed.set_footer(text='#' + ctx.channel.name)
            embed.set_author(name=ctx.author.name, icon_url=ctx.author.display_avatar.replace(format='png', size=128).url)
        else:
            embed.set_footer(text=ctx.bot.i18n.localize("reason", ctx=ctx).format(ctx.author))
    return embed


Pingable = typing.Union[commands.Context, discord.Message, discord.abc.Snowflake, discord.abc.User, int, str]


def ping_user(users: typing.Union[typing.List[Pingable], Pingable]) -> discord.AllowedMentions:
    """Returns an AllowedMentions instance of the author(s) or snowflake-type"""
    pings = []
    if not isinstance(users, list):
        users = [users]
    for usr in users:
        if isinstance(usr, discord.ext.commands.Context) or isinstance(usr, discord.Message):
            pings.append(usr.author)
        elif isinstance(usr, discord.abc.Snowflake):
            pings.append(usr)
        elif isinstance(usr, str):
            pings.append(discord.Object(int(usr)))
        elif isinstance(usr, int):
            pings.append(discord.Object(usr))
        else:
            raise ValueError("Unknown input type")  # no easy i18n method (sorry foreign bot hosters??)
    return discord.AllowedMentions(everyone=False, users=pings, roles=False)


async def private_message(ctx: commands.Context, msg_to_send: str):
    """Privately messages command output"""
    if isinstance(ctx.channel, discord.DMChannel):
        await ctx.send(msg_to_send)
        return
    try:
        await ctx.author.send(msg_to_send)
        perms = ctx.channel.permissions_for(ctx.guild.me)
        emoji = get_constant_emoji('DM_SENT', ctx.channel)
        if perms.add_reactions and perms.read_message_history:
            await ctx.message.add_reaction(emoji)
        else:
            await ctx.send(ctx.bot.i18n.localize("utils_dm", ctx=ctx).format(emoji), delete_after=7)
    except discord.Forbidden:
        await ctx.send(ctx.bot.i18n.localize("utils_dm_fail", ctx=ctx).format(ctx.author.mention), delete_after=15)


def comma_separator(lst: list, oxford_comma: bool = True) -> str:
    """Creates a comma separated list, ie 'Joe, Bob, and Frank'"""
    length = len(lst)
    if length > 1:
        output_str = ', '.join(lst[:-1])
        comma = ',' if length > 2 and oxford_comma else ''
        output_str = output_str + comma + ' & ' + lst[length - 1]
    else:
        output_str = ''.join(lst)
    return output_str


def clamp(n: int, minimum: int, maximum: int) -> int:
    """Clamps an input n to fit in range [minimum, maximum]"""
    return max(minimum, min(n, maximum))


def unpack_args(ctx) -> tuple:
    return ctx.args[2:]  # gets direct command inputs, removing the 'self' and 'ctx'


def error_gen(ctx: commands.Context, error_message: str) -> discord.Embed:
    """Returns an embed containing the input error message and a footer noting the command requester."""
    embed = embed_author_template(ctx, subtle_author=True)
    embed.description = error_message
    embed.colour = discord.Colour.red()
    return embed


def data_path(filename: str):
    return os.path.join(CONSTANTS['data_folder'], filename)


def shorten(string: str, limit: int = 2000):
    # TODO: find some way to add i18n??
    if len(string) <= limit:
        return string
    ending = "[...]"  # if ctx is None else ctx.bot.i18n.localize('too_long', ctx=ctx)
    return string[:limit - len(ending)] + ending


async def delete_after(message: discord.Message, delay: float = CONSTANTS['self_delete']):
    # perm checks technically arent needed BUT they save on ratelimits
    if not message.guild:
        return
    if message.channel.permissions_for(message.guild.me).manage_messages:
        await message.edit(delete_after=delay)


def get_roles_by_prefix(prefix, source: typing.Union[discord.Guild, discord.Member]):
    top_role = source.me.top_role if isinstance(source, discord.Guild) else source.guild.me.top_role
    return list(filter(lambda x: x.name.startswith(prefix) and top_role > x, source.roles))


def get_guild_names(guilds):
    return comma_separator([f'`{esc_md(guild.name)}`' for guild in guilds])


async def set_role(member: discord.Member, role: discord.Role, add_role: bool, has_role: bool = None):
    if has_role is None:
        has_role = role in member.roles

    func = None
    if not has_role and add_role:
        func = member.add_roles
    elif has_role and not add_role:
        func = member.remove_roles

    if func:
        await func(role)


async def toggle_role(member: discord.Member, role: discord.Role):
    has_role = role in member.roles
    await set_role(member, role, not has_role, has_role)


def add_attachment_info(embed: discord.Embed, message: discord.Message, i18n: Localizer):
    _ = i18n.localize
    _p = i18n.localize_p

    attachment_added = False
    misc = {'attachment': 0, 'embed': len(message.embeds)}
    for attachment in message.attachments:
        is_image_file = attachment.filename.split('.')[::-1][0].lower() in CONSTANTS['image_extensions']
        if attachment_added or not is_image_file or attachment.is_spoiler():
            misc['attachment'] += 1
        elif is_image_file:
            embed.set_image(url=attachment.url)
            attachment_added = True
    # display note if extra attachments/embeds couldn't be viewed
    embed_field = []
    for key, count in misc.items():
        if count > 0:
            embed_field.append(_p('everyboard_not_shown_'+key, count, guild=message.guild).format(count))
    if embed_field:
        embed.add_field(name=_('everyboard_not_shown_name', guild=message.guild),
                        value='\n'.join(embed_field))
    return embed


async def async_test(*args, **kwargs):
    print('hey!')
    print(args)
    print(kwargs)
    if 'hi' in args:
        raise Exception("oopsie woopsie")
    print('epic')
    return True


def get_or_default(obj: typing.Union[typing.Dict, typing.Any], field: str, default: typing.Any = None):
    if isinstance(obj, MutableMapping) and field in obj:
        return obj[field]
    if hasattr(obj, field):
        return getattr(obj, field)
    return default


def init_defaults(dest: dict, copy_from: dict) -> dict:
    for k, v in copy_from.items():
        if isinstance(v, dict):
            if k in dest:
                sub_dict = dest[k]
            else:
                sub_dict = {}
            v = init_defaults(sub_dict, v)
        dest.setdefault(k, v)
    return dest


class Null:
    def __bool__(self):
        return False

    def __str__(self):
        return "Null"

    def __repr__(self):
        return str(self)

    def __int__(self):
        return 0
