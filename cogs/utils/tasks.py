import pickle
import traceback
import uuid
from datetime import datetime as dt, timezone
from discord.utils import utcnow
from typing import TYPE_CHECKING

from cogs.utils.sql import TableManager

if TYPE_CHECKING:  # evaluates to False on runtime, avoiding the import
    from cogs.utils.bot import Semicolon


class ScheduledTask:
    """
    Contains the data for a task that should be executed at a specific point in time.
    Runs a function with an arbitrary number of inputs.
    """
    def __init__(self, bot: 'Semicolon', run_at: dt, func, *args, **kwargs):
        self.bot = bot
        self.id = uuid.uuid1()
        self.run_at = run_at
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def set_id(self, db_id) -> 'ScheduledTask':
        # workaround to set id when loading from db
        self.id = db_id
        return self

    @classmethod
    def from_db(cls, bot: 'Semicolon', db_id: str, run_at: int, func, args=None, kwargs=None):
        c_arg = (bot, dt.fromtimestamp(run_at, timezone.utc), pickle.loads(func))
        if args:
            c_arg += pickle.loads(args)
        c_kwargs = {} if kwargs is None else pickle.loads(kwargs)
        return cls(*c_arg, **c_kwargs).set_id(uuid.UUID(hex=db_id))

    def register(self, db: TableManager):
        try:
            cols = ['id', 'run_at', 'func']
            data = (self.id.hex, round(self.run_at.timestamp()), pickle.dumps(self.func))
            if self.args:
                cols.append('args')
                data += (pickle.dumps(self.args),)
            if self.kwargs:
                cols.append('kwargs')
                data += (pickle.dumps(self.kwargs),)
            db.create_row(data, cols)
        except:
            traceback.print_exc()

    def is_ready(self):
        return dt.now(timezone.utc) >= self.run_at

    async def execute(self):
        return await self.func(self.bot, *self.args, **self.kwargs)

    def __str__(self):
        args = ','.join(map(repr, self.args))
        kwargs = ','.join((f"{key}={repr(value)}" for key, value in self.kwargs.items()))
        return f"{self.func.__name__}({','.join([args, kwargs])})"

    def __repr__(self):
        return self.__str__()
