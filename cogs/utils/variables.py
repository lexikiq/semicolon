import typing

import discord
from discord.ext import commands

from cogs.utils import utils

if typing.TYPE_CHECKING:
    from cogs.utils.bot import Semicolon


def display_variable(data) -> str:
    if hasattr(data, 'mention'):
        data = getattr(data, 'mention')
    elif hasattr(data, 'id'):
        data = str(getattr(data, 'id'))
    elif isinstance(data, type):
        data = data.__name__
    elif isinstance(data, list):
        data = utils.comma_separator([str(display_variable(x)) for x in data])
    return data


class FakeContext:
    def __init__(self,
                 bot: 'Semicolon' = None,
                 guild: discord.Guild = None,
                 channel: typing.Union[discord.TextChannel, discord.DMChannel] = None,
                 author: discord.Member = None):
        self.bot = bot
        self.guild = guild
        self.channel = channel
        self.author = author


ContextLike = typing.Union[FakeContext, commands.Context]
