from multiprocessing.sharedctypes import Value
import typing

import discord
from discord.ext import commands

from .utils import shorten
from .constants import CONSTANTS
if typing.TYPE_CHECKING:  # evaluates to False on runtime, avoiding the import
    from .bot import Semicolon
    from .variables import FakeContext


class UserCheckedView(discord.ui.View):
    def __init__(self, user_id: int, *args, **kwargs):
        self.user_id = user_id
        super().__init__(*args, **kwargs)

    async def interaction_check(self, interaction: discord.Interaction, /) -> bool:
        return interaction.user.id == self.user_id

class ButtonOptionArgs(typing.NamedTuple):
    id: typing.Optional[str] = None
    label: typing.Optional[str] = None
    style: discord.ButtonStyle = discord.ButtonStyle.secondary
    emoji: typing.Union[discord.PartialEmoji, discord.Emoji, str, None] = None
    disabled: bool = False
    url: typing.Optional[str] = None

class MenuOptionArgs(typing.NamedTuple):
    id: str
    label: str
    description: typing.Optional[str] = None
    emoji: typing.Union[discord.PartialEmoji, discord.Emoji, str, None] = None
    preview_text: typing.Optional[str] = None

async def prompt(
    ctx: typing.Union[commands.Context['Semicolon'], 'FakeContext'],
    prompt: str,
    *,
    menu_options: typing.Optional[typing.List[MenuOptionArgs]] = None,
    button_options: typing.Optional[typing.List[ButtonOptionArgs]] = None,
    placeholder: typing.Optional[str] = None,
    timeout: float = 60.0,
    delete_invoking_message: bool = True,
    dest: typing.Optional['discord.abc.Messageable'] = None):

    if menu_options is None and button_options is None:
        raise ValueError('At least one of menu_options and button_options must be specified')

    if dest is None:
        if ctx.guild:
            perms = ctx.channel.permissions_for(ctx.guild.me)
            dest = ctx.channel if perms.send_messages else ctx.author # send message to DMs if no chatting perms
        else:
            dest = ctx.author

    response_id = None
    responded = False
    # Reassigned every time a new page is selected
    contents: typing.Optional[typing.Tuple[UserCheckedView, typing.Optional[discord.Embed]]] = None

    page_item_limit = 25
    page_index = 0
    if menu_options is not None:
        previews = [option.preview_text for option in menu_options if option.preview_text is not None]
        if len(previews) != len(menu_options) and len(previews) != 0:
            raise ValueError('Either every option must have preview text or none of them can')

        # Ceiling division
        num_pages = -(len(menu_options) // -page_item_limit)

    class CallbackSelect(discord.ui.Select):
        async def callback(self, interaction: discord.Interaction):
            nonlocal response_id, responded, contents
            response_id = self.values[0]
            responded = True
            if interaction.message:
                await interaction.message.delete()
            if delete_invoking_message and isinstance(ctx, commands.Context) and ctx.message.guild.me.guild_permissions.manage_messages:
                await ctx.message.delete()
            if contents:
                contents[0].stop()

    class CallbackButton(discord.ui.Button):
        async def callback(self, interaction: discord.Interaction):
            nonlocal response_id, responded, contents
            response_id = self.custom_id
            responded = True
            if interaction.message:
                await interaction.message.delete()
            if delete_invoking_message and isinstance(ctx, commands.Context) and ctx.message.guild.me.guild_permissions.manage_messages:
                await ctx.message.delete()
            if contents:
                contents[0].stop()

    class PageButton(discord.ui.Button):
        async def callback(self, interaction: discord.Interaction):
            nonlocal page_index, contents
            if self.custom_id == "prev":
                page_index = max(0, page_index - 1)
            else:
                page_index = min(page_index + 1, num_pages - 1)
            if contents:
                contents[0].stop()
            contents = make_page(page_index)
            await interaction.response.edit_message(view=contents[0], embed=contents[1])

    def make_page(index: int):
        view = UserCheckedView(user_id=ctx.author.id, timeout=timeout)
        embed = None

        if menu_options is not None:
            page_options = menu_options[index * page_item_limit:(index + 1) * page_item_limit]

            # This is needed for color roles specifically, since menu items don't support Markdown, and being able to see
            # the roles' colors is kinda the entire point.
            if len(previews):
                page_previews = previews[index * page_item_limit:(index + 1) * page_item_limit]
                embed = discord.Embed(color=CONSTANTS['colors']['default'])
                embed.set_footer(text=ctx.bot.i18n.localize('pagination_page', ctx=ctx).format(page_index + 1, num_pages),
                                icon_url=ctx.author.display_avatar.replace(format='png', size=128).url)
                embed.description = "\n".join(page_previews).strip()

            view.add_item(CallbackSelect(placeholder=placeholder, options=[
                discord.SelectOption(
                    label=shorten(item.label, 100),
                    value=item.id,
                    description=shorten(item.description, 100) if item.description else None,
                    emoji=item.emoji)
            for item in page_options]))

            if num_pages > 1:
                view.add_item(PageButton(emoji="\N{LEFTWARDS BLACK ARROW}", custom_id="prev", disabled=index == 0))
                # It's probably not *great* to use a button as a label, but having the components all in a row looks so nice
                view.add_item(PageButton(label=ctx.bot.i18n.localize('pagination_page', ctx=ctx).format(page_index + 1, num_pages), custom_id="page", disabled=True))
                view.add_item(PageButton(emoji="\N{BLACK RIGHTWARDS ARROW}", custom_id="next", disabled=index == num_pages - 1))

        if button_options is not None:
            for id, label, style, emoji, disabled, url in button_options:
                view.add_item(CallbackButton(
                    custom_id=id,
                    style=style,
                    label=label,
                    disabled=disabled,
                    emoji=emoji,
                    url=url,
                    # Position below pagination buttons
                    row=2))
        return view, embed

    contents = make_page(page_index)
    # this is really dumb, but is needed for the typechecker
    if contents[1] is None:
        await dest.send(prompt, view=contents[0])
    else:
        await dest.send(prompt, view=contents[0], embed=contents[1])

    while True:
        # Wait for either a button to be pressed or an option to be selected
        timed_out = await contents[0].wait()
        if timed_out:
            return None
        # If an option was selected, return that. Otherwise, loop again.
        if responded:
            return response_id
