import logging
import os
import re
import typing
import unicodedata
from abc import ABCMeta
from datetime import datetime, timedelta, timezone
from pathlib import Path
import parsedatetime as pdt

import discord
from discord.ext import commands
from discord.utils import escape_markdown as esc_md

from cogs.utils.errors import ConverterInitError, UserInputError
from cogs.utils.interactions import prompt, ButtonOptionArgs
from cogs.utils.utils import data_path, Null
from cogs.utils.variables import ContextLike

if typing.TYPE_CHECKING:  # evaluates to False on runtime, avoiding the import
    from cogs.utils.bot import Semicolon

index_folder = data_path("index")
if not os.path.exists(index_folder):
    os.makedirs(index_folder)
else:
    index_expire = timedelta(days=30)

    for path, folders, files in os.walk(index_folder):
        # ignore subfolders
        if path != index_folder:
            continue
        # get all types of files based on existence of their lock file
        lock_files = filter(lambda x: x.endswith('_WRITELOCK'), files)

        for file in lock_files:
            full_path = os.path.join(path, file)

            p_obj = Path(full_path)
            dt = datetime.fromtimestamp(p_obj.stat().st_mtime)  # file modified datetime

            # if file was last modified too long ago, delete all the related files
            if (datetime.now() - dt) > index_expire:
                f_group = file[:-len("_WRITELOCK")]
                logging.getLogger("bot").info("Clearing index files for "+f_group)
                related_files = filter(lambda x: re.match(f"_?{f_group}_", x), files)
                for del_file in related_files:
                    os.remove(os.path.join(path, del_file))


return_types = typing.Union[discord.Object, discord.Guild, discord.abc.Messageable, discord.Role, discord.Emoji,
                            discord.abc.GuildChannel, Null]


class ObjectType:
    def __str__(self):
        return type(self).__name__.replace('_', ' ').title()


class USER(ObjectType): pass
class MEMBER(USER): pass
class GUILD(ObjectType): pass
class CHANNEL(ObjectType): pass
class TEXT_CHANNEL(CHANNEL): pass
class VOICE_CHANNEL(CHANNEL): pass
class CATEGORY(CHANNEL): pass
class EMOJI(ObjectType): pass
class ROLE(ObjectType): pass

T = typing.TypeVar('T')

class BaseConverter(commands.Converter[T], metaclass=ABCMeta):
    def __str__(self):
        return type(self).__name__.replace('_', ' ').title()

    def get_i18n_name(self, ctx: commands.Context):
        return ctx.bot.i18n.localize('converter_'+type(self).__name__, ctx=ctx)


class ConverterWrapper(BaseConverter, metaclass=ABCMeta):
    def __init__(self, converter):
        self.converter = converter
        self.is_converter = isinstance(converter, BaseConverter)

    def __str__(self):
        return super().__str__() + ' ' + str(self.converter)

    def _get_sub_name(self, ctx: commands.Context):
        conv = self.converter
        if hasattr(conv, 'get_i18n_name'):
            return conv.get_i18n_name(ctx)
        elif isinstance(conv, type):
            return conv.__name__
        else:
            return str(conv)

    def get_i18n_name(self, ctx: commands.Context):
        return super().get_i18n_name(ctx).format(self._get_sub_name(ctx))

    async def convert_arg(self, ctx, argument, **params):
        if self.is_converter:
            return await self.converter.convert(ctx, argument, **params)
        else:
            try:
                return self.converter.__new__(self.converter, argument)
            except ValueError:
                raise UserInputError(str(ValueError))


class List(ConverterWrapper):
    def __init__(self, converter, separator: str = ' '):
        super().__init__(converter)
        self.separator = separator

    def __str__(self):
        f_args = (str(self.converter), self.separator, unicodedata.name(self.separator))
        return "List of {} (separated by `{}` `{}`)".format(*f_args)

    def get_i18n_name(self, ctx: commands.Context):
        f_args = (self._get_sub_name(ctx), self.separator, unicodedata.name(self.separator))
        return ctx.bot.i18n.localize('converter_'+type(self).__name__, ctx=ctx).format(*f_args)

    async def convert(self, ctx: commands.Context, argument: str, **params) -> \
            typing.Union[typing.List[typing.Any], str]:
        if isinstance(argument, str):  # for when setting from user input
            return [await self.convert_arg(ctx, x, **params) for x in argument.split(self.separator)]
        return argument  # for when getting

        # NOTE: really, these converter classes should be refactored to have a getter and setter method;
        # setter for interpreting from user's command input, getter for interpreting from the config.
        # as such, this solution is a bit of a bodge, but I think it's not so bad considering the bodge is only used
        # for this List class and the Bool class (the use of __str__ is a workaround to achieve a similar result)


class Bool(BaseConverter):
    async def convert(self, ctx: commands.Context, argument: str, **params):
        # using __str__ because cogs.utils.bot.Semicolon.interpret_data() uses this convert method
        # which, in the case of bool, raises `AttributeError: 'bool' object has no attribute 'lower'`.
        # so instead I just explicitly convert to string to get around this
        if argument.__str__().lower() in ['true', 't', 'y']:
            return True
        elif argument.__str__().lower() in ['false', 'f', 'n']:
            return False
        else:
            raise UserInputError(str(ValueError))
        # TODO: support "true" and "false" in locales
        # - add new i18n keys: `boolean_true` = Array<str>, `boolean_false` = Array<str>


class Nullable(ConverterWrapper):
    async def convert(self, ctx: commands.Context, argument: str, **params):
        if str(argument).lower() in ['null', 'none', 'false']:
            return Null()

        return await self.convert_arg(ctx, argument, **params)


async def _confirm(ctx: commands.Context['Semicolon'], obj, searched: typing.Set) -> bool:
    if obj.id in searched:
        return False
    searched.add(obj.id)
    obj_name = esc_md(str(obj))
    if hasattr(obj, 'mention'):
        obj_name = f"{obj.mention} ({obj_name})"
    text = ctx.bot.i18n.localize('snowflake_confirm', ctx=ctx).format(ctx.author.mention, obj_name)
    return await prompt(ctx, text, button_options=[
        ButtonOptionArgs('yes', ctx.bot.i18n.localize('yn_yes', ctx=ctx), discord.ButtonStyle.success),
        ButtonOptionArgs('no', ctx.bot.i18n.localize('yn_no', ctx=ctx), discord.ButtonStyle.secondary),
    ]) == 'yes'


class SnowflakeConvert(BaseConverter):
    snowsearch = re.compile(r"(?:<[@#:][!&]?(?:.{2,32}?:)?)?(\d{17,19})>?")
    namesearch = re.compile(r"@?(.{2,32})#(\d{4})")

    def __init__(self, object_type: typing.Union[ObjectType, typing.Type[ObjectType]], allow_partial: bool = True):
        if isinstance(object_type, ObjectType):
            object_type = type(object_type)
        self.object_type = object_type
        self.allow_partial = allow_partial

    def __str__(self):
        return self.object_type.__name__.replace('_', ' ').title()

    def get_i18n_name(self, ctx: commands.Context):
        return ctx.bot.i18n.localize('converter_' + type(self).__name__ + '_' + self.object_type.__name__)

    def obj_init(self, ctx: ContextLike) -> typing.Tuple[typing.Callable[[int], return_types], typing.List[return_types]]:
        # TODO: why is this hardcoded lol
        if issubclass(self.object_type, MEMBER):
            get_obj = ctx.guild.get_member
            all_obj = ctx.guild.members
        elif issubclass(self.object_type, USER):
            get_obj = ctx.bot.get_user
            all_obj = ctx.bot.users
        elif issubclass(self.object_type, CHANNEL):
            get_obj = lambda c: ctx.guild.get_channel(c) or ctx.guild.get_thread(c)
            if issubclass(self.object_type, CATEGORY):
                all_obj = ctx.guild.categories
            elif issubclass(self.object_type, TEXT_CHANNEL):
                chans: typing.List[discord.abc.Messageable] = list(ctx.guild.channels)
                chans.extend(ctx.guild.threads)
                all_obj = filter(lambda c: isinstance(c, discord.abc.Messageable), chans)
            elif issubclass(self.object_type, VOICE_CHANNEL):
                all_obj = filter(lambda c: isinstance(c, discord.channel.VocalGuildChannel), ctx.guild.channels)
            else:
                all_obj = getattr(ctx.guild, self.object_type.__name__.lower() + 's')
        elif issubclass(self.object_type, GUILD):
            get_obj = ctx.bot.get_guild
            all_obj = ctx.bot.guilds
        elif issubclass(self.object_type, EMOJI):
            get_obj = ctx.bot.get_emoji
            all_obj = ctx.bot.emojis
        elif issubclass(self.object_type, ROLE):
            get_obj = ctx.guild.get_role
            all_obj = ctx.guild.roles
        else:
            raise ConverterInitError("Unknown object type")
        return get_obj, all_obj

    async def convert(self, ctx: ContextLike, argument: str, ask_confirmation: bool = True):
        argument = str(argument)
        real_ctx = isinstance(ctx, commands.Context)
        if not real_ctx:
            assert ctx.guild is not None
            assert ctx.bot is not None
        else:
            await ctx.typing()

        # whether to use all_obj to find the object by ID instead (via discord.utils.get)
        alt_get = issubclass(self.object_type, (TEXT_CHANNEL, VOICE_CHANNEL, CATEGORY))

        # initialize getter objects and list objects
        get_obj, all_obj = self.obj_init(ctx)
        objname = self.object_type.__name__
        is_member = issubclass(self.object_type, MEMBER)  # save some slight processing
        exc = commands.BadArgument(ctx.bot.i18n.localize('snowflake_error', guild=ctx.guild).format(objname))

        if result := self.snowsearch.fullmatch(argument):
            snowflake = int(result.group(1))
            if not alt_get:
                obj = get_obj(snowflake)
            else:
                obj = discord.utils.get(all_obj, id=snowflake)
            if obj:
                return obj
            if self.allow_partial:
                return discord.Object(snowflake)
        if issubclass(self.object_type, USER) and (result := self.namesearch.fullmatch(argument)) and \
           (obj := discord.utils.get(all_obj, name=result.group(1), discriminator=result.group(2))):
            return obj

        if not ask_confirmation or not real_ctx:
            raise exc

        ctx: commands.Context
        searched: typing.Set[int] = set()

        search_str = argument
        if (issubclass(self.object_type, CHANNEL) and not issubclass(self.object_type, (VOICE_CHANNEL, CATEGORY))
            and search_str.startswith('#')) \
                or (issubclass(self.object_type, (USER, ROLE)) and search_str.startswith('@')):
            search_str = search_str[1:]

        search_str = search_str.lower()

        # username equals
        if (obj := discord.utils.find(lambda x: x.name.lower() == search_str, all_obj))\
                and await _confirm(ctx, obj, searched):
            return obj

        # nickname equals
        if (obj := discord.utils.find(lambda x: x.nick and x.nick.lower() == search_str, all_obj))\
                and await _confirm(ctx, obj, searched):
            return obj

        # username starts with
        if (obj := discord.utils.find(lambda x: x.name.lower().startswith(search_str), all_obj)) \
                and await _confirm(ctx, obj, searched):
            return obj

        # nickname starts with
        if (obj := discord.utils.find(lambda x: x.nick and x.nick.lower().startswith(search_str),
                                      all_obj)) and await _confirm(ctx, obj, searched):
            return obj

        # give up
        raise exc


# I'd implement a proper Dictionary class but this project is basically unmaintained so no point in
# over-engineering a solution to this one problem
class Int2IntDict(BaseConverter):
    async def convert(self, ctx: ContextLike, argument: str):
        argument = str(argument)
        try:
            return {int(k): int(v) for k, v in (x.split('=') for x in argument.split(','))}
        except ValueError:
            raise commands.BadArgument(ctx.bot.i18n.localize('int2int_error'))


units = pdt.pdtLocales['en_US'].units
units['minutes'].append('mins')
units['seconds'].append('secs')
p = pdt.Calendar()

class ConvertedTime(typing.NamedTuple):
    time: datetime
    when: datetime

class TimeConvert(BaseConverter[ConvertedTime]):
    def __init__(self, *, allow_past=False):
        self.allow_past = allow_past

    async def convert(self, ctx: commands.Context['Semicolon'], argument: str):
        time = str(argument)
        when = ctx.message.created_at.replace(microsecond=0)
        time_struct, parse_status = p.parse(time, when)
        if not parse_status:
            raise commands.BadArgument(ctx.bot.i18n.localize('converter_time_invalid_input', ctx=ctx))
        dt = datetime(*time_struct[:6], tzinfo=timezone.utc)
        if not self.allow_past and dt < when:
            raise commands.BadArgument(ctx.bot.i18n.localize('converter_time_is_in_the_past', ctx=ctx))
        return ConvertedTime(dt, when)