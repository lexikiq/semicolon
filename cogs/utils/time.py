from datetime import datetime, timezone

import discord
from discord.utils import utcnow
import humanize

from cogs.utils.bot import Semicolon


async def remind(bot: Semicolon, author_id: int, message_id: int, message: str):
    user: discord.User = bot.get_user(author_id)
    if user is None:
        return
    stamp = int(discord.Object(message_id).created_at.timestamp())
    fmt_stamp = f"<t:{stamp}:R>"
    msg = bot.i18n.localize('reminder', user=author_id).format(fmt_stamp, message)
    try:
        await user.send(msg)
    except discord.Forbidden:
        pass


async def unmute(bot: Semicolon, user_id: int, guild_id: int, role_id: int):
    guild: discord.Guild = bot.get_guild(guild_id)
    if guild is None:
        return

    me: discord.Member = guild.me
    if not me.guild_permissions.manage_roles:
        return

    role: discord.Role = guild.get_role(role_id)
    if role is None:
        return
    if role >= me.top_role:
        return

    member: discord.Member = guild.get_member(user_id)
    if member is None:
        try:
            member = await guild.fetch_member(user_id)  # throws on NotFound
        except discord.NotFound:
            bot.logger.info(f"[Unmute] Could not find muted user <@{user_id}> in guild {guild.name} (Role: {role.mention})")

    if role in member.roles:
        await member.remove_roles(role, reason=bot.i18n.localize("unmute_reason", guild=guild_id))
