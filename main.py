from cogs.utils.bot import Semicolon
from asyncio import run


async def main():
    bot = Semicolon()
    await bot.run()


if __name__ == '__main__':
    run(main())
